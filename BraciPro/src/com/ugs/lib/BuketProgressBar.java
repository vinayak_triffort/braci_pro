package com.ugs.lib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class BuketProgressBar extends ProgressBar {

	int m_value = 0;
	
	Paint m_paint;
	Rect arcRect;
	
	public BuketProgressBar(Context context) {
		super(context);
		init();
	}

	public BuketProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public BuketProgressBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	
	private void init() {
		m_value = 0;
		
		m_paint = new Paint();
		m_paint.setColor(0xff52a7c6);
		m_paint.setAntiAlias(true);
		m_paint.setStyle(Style.FILL);	
		
		arcRect = new Rect();
	}

	public void setValue(int p_Value) {
		m_value = p_Value;
		invalidate();
	}

	protected synchronized void onDraw(Canvas canvas) {

		int progress = m_value; // this.getProgress();
		int maxProgress = this.getMax();

		// get the drawing rect of your progress bar layout object.
		// This way you can change the size in layout files (easier support for
		// multiple screen size)

		this.getDrawingRect(arcRect);
		int height = arcRect.height();
		arcRect.top = arcRect.bottom - height * progress / maxProgress;
		
		canvas.drawRect(arcRect, m_paint);
	}
}
