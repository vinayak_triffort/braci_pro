package com.ugs.lib;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class CircularProgressBar extends ProgressBar {

	int m_value = 0;

	public CircularProgressBar(Context context) {
		super(context);
		m_value = 0;
	}

	public CircularProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		m_value = 0;
	}

	public CircularProgressBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		m_value = 0;
	}

	public void setValue(int p_Value) {
		m_value = p_Value;
		invalidate();
	}

	protected synchronized void onDraw(Canvas canvas) {

		int progress = m_value; // this.getProgress();
		int maxProgress = this.getMax();

		// calc the progress to angles
		float angleProgress = progress * 360 / maxProgress;

		// create and set the arc paint
		Paint arcPaint = new Paint();
		arcPaint.setColor(0xff33A1E1);
		arcPaint.setAntiAlias(true);
		arcPaint.setStyle(Style.FILL);

		// get the drawing rect of your progress bar layout object.
		// This way you can change the size in layout files (easier support for
		// multiple screen size)
		Rect arcRect = new Rect();
		this.getDrawingRect(arcRect);
		int width = arcRect.width();
		int height = arcRect.height();
		arcRect.left = arcRect.left + width * 32 / 400;
		arcRect.right = arcRect.right - width * 32 / 400;
		arcRect.top = arcRect.top + height * 32 / 400;
		arcRect.bottom = arcRect.bottom - height * 32 / 400;

		// Draw the Arc into that rect
		canvas.drawArc(new RectF(arcRect), 90, angleProgress, true, arcPaint);

	}
}
