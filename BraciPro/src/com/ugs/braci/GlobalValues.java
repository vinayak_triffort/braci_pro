package com.ugs.braci;

import java.util.ArrayList;

import com.uc.prjcmn.PRJCONST;
import com.ugs.braci.engine.DetectingData;
import com.ugs.braci.engine.FFTView;
import com.ugs.braci.engine.SoundEngine;
import com.ugs.equalizer.EqualizerView;
import com.ugs.service.BraciProService;

public class GlobalValues {
	public static float DEVICE_SENSITIVITY = 1.0f; // Default Sensitivity for
													// other devices
	public static float SENSITIVITY_SAMSUNG = 1.0f; // sensitivity for SAMSUNG
	public static float SENSITIVITY_LGE = 1.0f; // sensitivity for LGE
	public static float SENSITIVITY_HTC = 1.0f; // sensitivity for HTC

	public static final int COMMAND_OPEN_ACITIVITY = 20032;
	public static final int COMMAND_ENABLE_DETECT = 20033;
	public static final int COMMAND_DISABLE_DETECT = 20034;
	public static final int COMMAND_REMOVE_NOTIFY = 20035;
	public static final int COMMAND_PUSH_NOTIFICATION = 20036;
	public static final int COMMAND_SEND_PEBBLE = 20037;
	public static final int COMMAND_SYSTEM_EXIT = 20038;
	public static final int COMMAND_PUSH_SENDING = 20039;
	
	public static final int COMMAND_MATCHED_PRODUCT = 25001;

	public static boolean _firstRun = false;

	public static FFTView _fftView = null;
	public static EqualizerView _equalizerView = null;
	public static SoundEngine _soundEngine = null;
	public static BraciProService _myService = null;

	public static int _frameCnt = 0;

	public static boolean _bEnabledActivity = false;

	public static boolean _bExistHomeActivity = false;

	public static boolean m_bDetect = false;
	public static boolean m_bPebbleWatch = true;

	// Alerting methods
	public static boolean m_bNotifyVibrate = true;
	public static boolean m_bNotifyFlash = true;
	public static boolean m_bNotifyPebbleWatch = true;
	public static boolean m_bNotifyAndroidWear = true;
	public static boolean m_bNotifyPhilipsHue = true;

	// Profile Mode
	public static boolean m_bProfileAutoSel = false;
	public static int m_nProfileIndoorMode = PRJCONST.PROFILE_MODE_HOME;
	public static int m_nProfileOutdoorMode = PRJCONST.PROFILE_MODE_DRIVE;
	public static boolean m_bProfileIndoor = true;

	// User Infos
	public static String m_stUserName = "";
	public static String m_stPassword = "";

	public static boolean m_bDetectionPhone = false;
	
	// Data to matching
	public static ArrayList<DetectingData> _matchingData;

	// Data to detect
	public static boolean[] _bRecordedSoundsDetectable = null;
	public static Object[] recordedDetectData = null;
	
	// //> Door Bell
	public static boolean _bDoorbellDetectable = true;
	public static ArrayList<DetectingData> _extraDoorbellDetectData = null;

	// //> Fire Alarm
	public static boolean _bFireAlarmDetectable = true;
	public static ArrayList<DetectingData> _fireDetectData = null;

	// //> Car Horn
	public static boolean _bCarHornDetectable = true;
	public static ArrayList<DetectingData> _carHornsDetectData = null;

	// //> Police
	public static boolean _bPoliceDetectable = true;
	public static ArrayList<DetectingData> _policeDetectData = null;
	
	// //> Traffic Tone
	public static boolean _bTrafficToneDetectable = true;
	public static ArrayList<DetectingData> _trafficToneDetectData = null;
	
	// //> Train Horn
	public static boolean _bTrainHornDetectable = true;
	public static ArrayList<DetectingData> _trainHornDetectData = null;
	
	// //> Microwave
	public static boolean _bMicrowaveDetectable = true;
	public static ArrayList<DetectingData> _microwaveDetectData = null;

	// //> Thief Alarm
	public static boolean _bThiefAlarmDetectable = true;
	public static ArrayList<DetectingData> _thiefDetectData = null;

	// //> Alarm Clock
	public static boolean _bAlarmClockDetectable = true;
	public static ArrayList<DetectingData> _alarmClockDetectData = null;

	// //> Phone Ringing Clock
	public static boolean _bPhoneRingDetectable = true;
	public static ArrayList<DetectingData> _phoneRingDetectData = null;

	// Locations
	public static double location_home_lat = 0.0;
	public static double location_home_lng = 0.0;

	public static double location_office_lat = 0.0;
	public static double location_office_lng = 0.0;
	
	// Deaf Soundtype
	public static int g_nCurDetectingDeafSndType = -1;
	public static int[] g_nCurDetectingDeafSndIndies = null;
	
	// number of detection
	public static int _nNumberOfDetection = 0;
	
	
}
