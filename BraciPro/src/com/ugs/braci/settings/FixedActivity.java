package com.ugs.braci.settings;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;

public class FixedActivity extends Activity implements OnClickListener {

	private final String TAG = "_FixedActivity";
	
	public static String FIX_TYPE = "fix_type";

	private Context mContext;
	
	private int m_nFixType;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fixed);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;
		
		m_nFixType = getIntent().getIntExtra(FIX_TYPE, PRJCONST.FIX_FALSE_ALARM);

		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBack();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}
		
		ImageView _iv = (ImageView) findViewById(R.id.iv_finish);
		_iv.setOnClickListener(this);
	}

	private void scaleView() {
		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void onBack() {
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_finish:
			onBack();
			break;

		default:
			break;
		}
		
	}
}
