package com.ugs.braci.settings;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;
import com.ugs.braci.adapter.UIListAdapter;
import com.ugs.info.UIListItemInfo;

public class RepairFragment extends Fragment implements OnClickListener {

	private AdvancedSettingsActivity mActivity;

	private ListView m_listItems;
	private UIListAdapter m_adapterItems;

	private TextView m_tvDescription;

	private Fragment m_oldFragment;
	private String m_strOldTitle;

	private final int ITEM_REPAIR_FALSEALARM = 0;
	private final int ITEM_REPAIR_MISSING = 1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_general, container,
				false);

		mActivity = (AdvancedSettingsActivity) getActivity();

		m_oldFragment = mActivity.m_curFragment;
		m_strOldTitle = mActivity.m_tvTitle.getText().toString();

		mActivity.m_curFragment = RepairFragment.this;
		mActivity.m_tvTitle.setText(mActivity
				.getString(R.string.setting_alert_title));

		if (m_oldFragment == null) {
			PRJFUNC.hideFragment(mActivity, mActivity.m_settingsFragment);
		} else {
			PRJFUNC.hideFragment(mActivity, m_oldFragment);
		}

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		return v;
	}

	@Override
	public void onDestroyView() {
		if (mActivity.m_curFragment == RepairFragment.this) {
			mActivity.m_curFragment = m_oldFragment;
			mActivity.m_tvTitle.setText(m_strOldTitle);

			if (mActivity.m_curFragment == null) {
				PRJFUNC.showFragment(mActivity, mActivity.m_settingsFragment);
			} else {
				PRJFUNC.showFragment(mActivity, m_oldFragment);
			}
		}

		super.onDestroyView();
	}

	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		itemInfo = new UIListItemInfo(R.drawable.ic_repair_falsealarm,
				mActivity.getString(R.string.repair_falsealert_title),
				mActivity.getString(R.string.repair_falsealert_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_repair_missing,
				mActivity.getString(R.string.repair_missingalarm_title),
				mActivity.getString(R.string.repair_missingalarm_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
		m_tvDescription
				.setText(mActivity.getString(R.string.repair_desc));

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UIListAdapter(mActivity,
				R.layout.list_item_title_and_desc, new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

//		View _v = v.findViewById(R.id.frm_btn_ok);
//		_v.setVisibility(View.VISIBLE);
//
//		ImageView _iv = (ImageView) v.findViewById(R.id.iv_ok);
//		_iv.setOnClickListener(this);

		refreshList();
	}

	UIListAdapter.Callback m_adapterCallback = new UIListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			mActivity.onRepairProfileSelected(p_nPosition);

			switch (p_nPosition) {
			case ITEM_REPAIR_FALSEALARM:
				break;
			case ITEM_REPAIR_MISSING:
				break;
			default:
				break;
			}
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_ok:
			mActivity.goBack();
			break;
		default:
			break;
		}

	}
}
