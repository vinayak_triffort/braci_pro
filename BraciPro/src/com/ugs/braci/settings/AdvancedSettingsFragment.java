package com.ugs.braci.settings;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;
import com.ugs.braci.adapter.UIListAdapter;
import com.ugs.info.UIListItemInfo;

public class AdvancedSettingsFragment extends Fragment {

	private AdvancedSettingsActivity mActivity;

	private ListView m_listItems;
	private UIListAdapter m_adapterItems;

	private TextView m_tvDescription;
	
	private final int ITEM_SETTINGS_RECORDED = 0;
	private final int ITEM_SETTINGS_ALERT = 1;
	private final int ITEM_SETTINGS_REPAIR = 2;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_general, container,
				false);

		mActivity = (AdvancedSettingsActivity) getActivity();

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			
		}

		return v;
	}

	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		itemInfo = new UIListItemInfo(R.drawable.ic_settings_recorded,
				mActivity.getString(R.string.setting_recorded_title),
				mActivity.getString(R.string.setting_recorded_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_settings_alert,
				mActivity.getString(R.string.setting_alert_title),
				mActivity.getString(R.string.setting_alert_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_settings_repair,
				mActivity.getString(R.string.setting_repair_title),
				mActivity.getString(R.string.setting_repair_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
		m_tvDescription.setVisibility(View.GONE);

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UIListAdapter(mActivity, R.layout.list_item_title_and_desc,
				new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		refreshList();
	}

	UIListAdapter.Callback m_adapterCallback = new UIListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			switch(p_nPosition) {
			case ITEM_SETTINGS_RECORDED:
				mActivity.onRecordedSounds();
				break;
			case ITEM_SETTINGS_ALERT:
				mActivity.onAlertingMethodSelected();
				break;
			case ITEM_SETTINGS_REPAIR:
				mActivity.onRepairMethodSelected();
				break;
			}
		}
	};
}
