package com.ugs.braci.settings;

import java.io.File;
import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.PRJFUNC.Signal;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.R;
import com.ugs.braci.adapter.UIListAdapter;
import com.ugs.braci.engine.SoundEngine;
import com.ugs.braci.setup.SetupActivity;
import com.ugs.info.UIListItemInfo;

public class RepairSubFragment extends Fragment implements OnClickListener {

	private AdvancedSettingsActivity mActivity;
	
	private ListView m_listItems;
	private UIListAdapter m_adapterItems;

	private TextView m_tvDescription;

	private Fragment m_oldFragment;
	private String m_strOldTitle;

	private SharedPreferencesMgr m_phoneDb = null;

	private int m_nProfileMode;
	private int m_nFixType;

	float[] m_signalMatchRateDelta;

	public Signal[] signalTypes = { Signal.DOOR_BELL, Signal.BACKDOOR_BELL, Signal.SMOKE_ALARM,
			Signal.MOBILE_RINGING, Signal.INTERCOM, Signal.CO2,
			Signal.ALARM_CLOCK, Signal.THIEF_ALARM, Signal.MICROWAVE };

	public void setFixType(int p_nFixType) {
		m_nFixType = p_nFixType;
	}

	public void setProfileMode(int p_nProfileMode) {
		m_nProfileMode = p_nProfileMode;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_general, container,
				false);

		mActivity = (AdvancedSettingsActivity) getActivity();

		m_oldFragment = mActivity.m_curFragment;
		m_strOldTitle = mActivity.m_tvTitle.getText().toString();

		mActivity.m_curFragment = RepairSubFragment.this;

		String[] strTitles = {
				getActivity().getString(R.string.repair_falsealert_title),
				getActivity().getString(R.string.repair_missingalarm_title) };

		if (m_nProfileMode == PRJCONST.PROFILE_MODE_HOME) {
			mActivity.m_tvTitle.setText(strTitles[m_nFixType] + "("
					+ getActivity().getString(R.string.word_home) + ")");
		} else if (m_nProfileMode == PRJCONST.PROFILE_MODE_OFFICE) {
			mActivity.m_tvTitle.setText(strTitles[m_nFixType] + "("
					+ getActivity().getString(R.string.word_office) + ")");
		}

		if (m_oldFragment == null) {
			PRJFUNC.hideFragment(mActivity, mActivity.m_settingsFragment);
		} else {
			PRJFUNC.hideFragment(mActivity, m_oldFragment);
		}

		updateLCD(v);

		m_phoneDb = new SharedPreferencesMgr(mActivity);
		m_signalMatchRateDelta = m_phoneDb
				.loadMatchingRatesDelta(m_nProfileMode);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		return v;
	}

	@Override
	public void onDestroyView() {
		if (mActivity.m_curFragment == RepairSubFragment.this) {
			mActivity.m_curFragment = m_oldFragment;
			mActivity.m_tvTitle.setText(m_strOldTitle);

			if (mActivity.m_curFragment == null) {
				PRJFUNC.showFragment(mActivity, mActivity.m_settingsFragment);
			} else {
				PRJFUNC.showFragment(mActivity, m_oldFragment);
			}
		}

		super.onDestroyView();
	}

	private int[] m_imgSoundIds = { R.drawable.ic_setup_doorbell, R.drawable.ic_setup_doorbell,
			R.drawable.ic_setup_smokealarm, R.drawable.ic_setup_landline,
			R.drawable.ic_setup_intercom, R.drawable.ic_setup_carbon,
			R.drawable.ic_setup_alarmclock, R.drawable.ic_setup_thief,
			R.drawable.ic_setup_microwave };

	private int[] m_nSoundTitleIds = { R.string.setup_doorbell_title, R.string.setup_backdoorbell_title,
			R.string.setup_smokealarm_title, R.string.setup_landline_title,
			R.string.setup_intercom_title, R.string.setup_carbon_title,
			R.string.setup_alarmclock_title, R.string.setup_thief_title,
			R.string.setup_microwave_title};

	private int[] m_nSoundContentIds = { R.string.setup_doorbell_desc, R.string.setup_backdoorbell_desc,
			R.string.setup_smokealarm_desc, R.string.setup_landline_desc,
			R.string.setup_intercom_desc, R.string.setup_carbon_desc,
			R.string.setup_alarmclock_desc, R.string.setup_thief_desc,
			R.string.setup_microwave_desc };

	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		for (int type = 0; type < PRJCONST.REC_SOUND_TYPE_CNT; type++) {
			if (SetupActivity.m_bSoundsRecordable[type]) {
				if (isExistSound(type)) {
					itemInfo = new UIListItemInfo(m_imgSoundIds[type],
							mActivity.getString(m_nSoundTitleIds[type]),
							mActivity.getString(m_nSoundContentIds[type]), true);
					itemInfo.setChecked(false);
					itemInfo.m_extraData = (Integer) type;
					m_adapterItems.add(itemInfo);
				}
			} else {
				itemInfo = new UIListItemInfo(m_imgSoundIds[type],
						mActivity.getString(m_nSoundTitleIds[type]),
						mActivity.getString(m_nSoundContentIds[type]), true);
				itemInfo.m_extraData = (Integer) type;
				itemInfo.setChecked(false);
				m_adapterItems.add(itemInfo);
			}
		}
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
		if (m_nFixType == PRJCONST.FIX_FALSE_ALARM) {
			m_tvDescription.setText(mActivity
					.getString(R.string.repair_falsealert_subdesc));
		} else {
			m_tvDescription.setText(mActivity
					.getString(R.string.repair_missingalarm_subdesc));
		}

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UIListAdapter(mActivity,
				R.layout.list_item_title_and_desc,
				new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		View _v = v.findViewById(R.id.frm_btn_ok);
		_v.setVisibility(View.VISIBLE);

		TextView _tv = (TextView) v.findViewById(R.id.tv_btn_text);
		_tv.setText(mActivity.getString(R.string.word_fix));

		ImageView _iv = (ImageView) v.findViewById(R.id.iv_ok);
		_iv.setOnClickListener(this);

		refreshList();
	}

	UIListAdapter.Callback m_adapterCallback = new UIListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			UIListItemInfo itemInfo = m_adapterItems.getItem(p_nPosition);
			itemInfo.m_bChecked = !itemInfo.m_bChecked;
			m_adapterItems.notifyDataSetChanged();
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_ok:
			onFix();
			break;

		default:
			break;
		}

	}

	private void onFix() {
		boolean bFixed = false;
		for (int i = 0; i < m_adapterItems.getCount(); i++) {
			UIListItemInfo itemInfo = m_adapterItems.getItem(i);
			if (itemInfo.m_bChecked) {
				fixSound(signalTypes[(Integer) itemInfo.m_extraData]);
				bFixed = true;
			}
		}

		if (!bFixed) {
			Toast.makeText(mActivity,
					mActivity.getString(R.string.repair_no_sounds),
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (m_nProfileMode == GlobalValues.m_nProfileIndoorMode) {
			SoundEngine.DELTA_MATCH_RATES = m_signalMatchRateDelta;
		}

		Intent intent = new Intent(mActivity, FixedActivity.class);
		intent.putExtra(FixedActivity.FIX_TYPE, m_nFixType);
		mActivity.startActivity(intent);
	}

	private void fixSound(Signal p_Signal) {
		if (p_Signal == Signal.SMOKE_ALARM || p_Signal == Signal.CO2) {
			int nUnivEngineType = convertRecTypeToUniEng(p_Signal);
			if (m_nFixType == PRJCONST.FIX_FALSE_ALARM) {
				SoundEngine.UNIVERSAL_THRESHOLDS[nUnivEngineType] += SoundEngine.DELTA_THRESHOLD_FOR_UNIVERSAL_ENGINE;
			} else if (m_nFixType == PRJCONST.FIX_MISSING_ALARM) {
				SoundEngine.UNIVERSAL_THRESHOLDS[nUnivEngineType] -= SoundEngine.DELTA_THRESHOLD_FOR_UNIVERSAL_ENGINE;
			}
			m_phoneDb.saveUnivEngThreshold(nUnivEngineType);
		} else {

			int nSignal = PRJFUNC.ConvSignalToInt(p_Signal);
			if (m_nFixType == PRJCONST.FIX_FALSE_ALARM) {
				m_signalMatchRateDelta[nSignal] += SoundEngine.DELTA_MATCH_RATE_FOR_CORRECTION;

			} else if (m_nFixType == PRJCONST.FIX_MISSING_ALARM) {
				m_signalMatchRateDelta[nSignal] -= SoundEngine.DELTA_MATCH_RATE_FOR_CORRECTION;
			}

			m_phoneDb.saveMatchingRateDelta(m_nProfileMode, nSignal,
					m_signalMatchRateDelta[nSignal]);
		}
	}

	private int convertRecTypeToUniEng(Signal p_signal) {
		if (p_signal == Signal.SMOKE_ALARM)
			return PRJCONST.UNIVERSAL_ENGINE_SMOKEALARM;
		if (p_signal == Signal.CO2)
			return PRJCONST.UNIVERSAL_ENGINE_CO2;
		return -1;
	}

	private boolean isExistSound(int p_nRecSndType) {
		String strFilePath = PRJFUNC
				.getRecordDir(p_nRecSndType, m_nProfileMode) + "/Detect.dat";
		File file = new File(strFilePath);
		if (file.exists())
			return true;
		return false;
	}

}
