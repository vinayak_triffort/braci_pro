package com.ugs.braci.help;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.photoslider.PhotoSliderActivity;
import com.ugs.braci.R;

public class TermsActivity extends Activity implements OnClickListener {

	private final String TAG = "_TermsActivity";

	private Context mContext;

	private CheckBox m_chkAgreed;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_terms);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goMainActivity();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		View v = findViewById(R.id.frm_check_agree);
		v.setOnClickListener(this);

		m_chkAgreed = (CheckBox) findViewById(R.id.ch_agreed);

		TextView _tv = (TextView) findViewById(R.id.tv_btn_accept);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);

		ImageView _iv = (ImageView) findViewById(R.id.iv_accept);
		_iv.setOnClickListener(this);
	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void goMainActivity() {
		finish();
		overridePendingTransition(R.anim.from_bottom, R.anim.hold);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_accept:
			onAccept();
			break;

		case R.id.frm_check_agree:
			m_chkAgreed.setChecked(!m_chkAgreed.isChecked());
			break;

		default:
			break;
		}

	}

	private void onAccept() {
		if (!m_chkAgreed.isChecked()) {
			AlertDialog.Builder dlgAlert = new AlertDialog.Builder(mContext);
			dlgAlert.setMessage("You must agree this terms.");
			dlgAlert.setTitle("Braci");
			dlgAlert.setPositiveButton("OK", null);
			dlgAlert.setCancelable(true);
			dlgAlert.create().show();
			return;
		}

		GlobalValues._firstRun = false;
		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mContext);
		phoneDb.setFirstRunTime(System.currentTimeMillis());
		
//		final Dialog myDialog = new Dialog(this);
//		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//		
//		myDialog.setContentView(R.layout.dlg_setup_alert);
//		
//       myDialog.setCancelable(false);
//       myDialog.show();
//       
//       Button continueAct = (Button)myDialog.findViewById(R.id.btnContinue);
//       Button buyFullVersionAct = (Button)myDialog.findViewById(R.id.btnBuyFullVersion);
//       
//       continueAct.setOnClickListener(new OnClickListener() {
//		
//		@Override
//		public void onClick(View v) {
//			// TODO Auto-generated method stub
//			
//		}
//	});
		
       Intent intent = null;
		intent = new Intent(TermsActivity.this, PhotoSliderActivity.class);
		startActivity(intent);
		finish();
	}

}
