package com.ugs.braci.help;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;

public class HelpAboutUsActivity extends Activity implements OnClickListener {

	private final String TAG = "_HelpAboutUsActivity";

	private Context mContext;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help_aboutus);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		PRJFUNC.testPebbleConnected(mContext);
		
		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBack();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		TextView _tv = (TextView) findViewById(R.id.tv_title);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);

		_tv = (TextView) findViewById(R.id.tv_content1);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);
		
		_tv = (TextView) findViewById(R.id.tv_period);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);
		
		_tv = (TextView) findViewById(R.id.tv_description);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSans);
	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void onBack() {
		finish();
	}

	@Override
	public void onClick(View v) {
//		switch (v.getId()) {
//
//		default:
//			break;
//		}
	}
}
