package com.ugs.braci.help;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;

public class HelpFeedbackActivity extends Activity implements OnClickListener {

	private final String TAG = "_HelpFeedbackActivity";

	private EditText m_etName;
	private EditText m_etContents;

	private Context mContext;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help_feedback);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		PRJFUNC.testPebbleConnected(mContext);

		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBack();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		TextView _tv = (TextView) findViewById(R.id.tv_title);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);

		_tv = (TextView) findViewById(R.id.tv_content1);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);

		ImageView _iv = (ImageView) findViewById(R.id.iv_send);
		_iv.setOnClickListener(this);
		
		m_etName = (EditText) findViewById(R.id.et_name);
		m_etContents = (EditText) findViewById(R.id.et_comments);
		
		int hintColor = Color.rgb(0xBf, 0xd9, 0xe4);
		m_etName.setHintTextColor(hintColor);
		m_etContents.setHintTextColor(hintColor);
	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void onBack() {
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_send:
			sendEmail();
			break;
		default:
			break;
		}
	}

	private void sendEmail() {
		String strName = m_etName.getText().toString();
		String strContents = m_etContents.getText().toString();

		if (strName.isEmpty()) {
			Toast.makeText(mContext, "Please input your name.",
					Toast.LENGTH_SHORT).show();
			return;
		}

		if (strContents.isEmpty()) {
			Toast.makeText(mContext, "Please input comments.",
					Toast.LENGTH_SHORT).show();
			return;
		}

		String[] recipients = { "Info@braci.co" };

		Intent email = new Intent(Intent.ACTION_SEND, Uri.parse("mailto:"));

		// prompts email clients only

		email.setType("message/rfc822");

		email.putExtra(Intent.EXTRA_EMAIL, recipients);

		email.putExtra(Intent.EXTRA_SUBJECT, "From " + strName);

		email.putExtra(Intent.EXTRA_TEXT, strContents);

		try {

			// the user can choose the email client

			startActivity(Intent.createChooser(email,
					"Choose an email client from..."));

		} catch (android.content.ActivityNotFoundException ex) {

			Toast.makeText(HelpFeedbackActivity.this,
					"No email client installed.",

					Toast.LENGTH_LONG).show();

		}
	}
}
