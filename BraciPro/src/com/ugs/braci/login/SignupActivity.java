package com.ugs.braci.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.R;

public class SignupActivity extends Activity implements OnClickListener {

	public static final boolean REQUIRED_EMAIL_VERIFIED = true;

	public static final String PARAM_FROM = "from";
	public static final int FROM_LOGIN = 0;
	public static final int FROM_PHOTOSLIDING = 1;

	private int m_nFrom;

	private final String TAG = "_SignupActivity";

	// private EditText m_etUsername;
	private EditText m_etEmail;
	private EditText m_etPwd;
	private EditText m_etPwdConfirm;

	private Context mContext;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_signup);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		m_nFrom = getIntent().getIntExtra(PARAM_FROM, FROM_LOGIN);

		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goBack();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		// m_etUsername = (EditText) findViewById(R.id.et_username);
		m_etEmail = (EditText) findViewById(R.id.et_email);
		m_etPwd = (EditText) findViewById(R.id.et_password);
		m_etPwdConfirm = (EditText) findViewById(R.id.et_verify_pwd);

		int hintColor = Color.rgb(0xB2, 0xC1, 0xC9);
		m_etEmail.setHintTextColor(hintColor);
		m_etPwd.setHintTextColor(hintColor);
		m_etPwdConfirm.setHintTextColor(hintColor);

		TextView _tv = null;

		_tv = (TextView) findViewById(R.id.tv_title);
		PRJFUNC.setTextViewFont(mContext, _tv, PRJCONST.FONT_OpenSansBold);

		ImageView _iv = (ImageView) findViewById(R.id.iv_signup);
		_iv.setOnClickListener(this);
	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void goBack() {
		if (m_nFrom == FROM_LOGIN) {
			setResult(RESULT_CANCELED);
			finish();
			overridePendingTransition(R.anim.from_bottom, R.anim.hold);
		} else {
			goToLoginActivity();
		}
	}

	private void goToLoginActivity() {
		Intent it = new Intent(SignupActivity.this, LoginActivity.class);
		startActivity(it);
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_signup:
			onSignup();
			break;

		default:
			break;
		}
	}

	private void onSignup() {
		// final String strUserName = m_etUsername.getText().toString();
		// if (strUserName.isEmpty()) {
		// Toast.makeText(mContext, "Please input username",
		// Toast.LENGTH_SHORT).show();
		// m_etUsername.requestFocus();
		// return;
		// }

		final String strEmail = m_etEmail.getText().toString();
		if (strEmail.isEmpty()) {
			Toast.makeText(mContext, "Please input E-mail", Toast.LENGTH_SHORT)
					.show();
			m_etEmail.requestFocus();
			return;
		}

		final String strPwd = m_etPwd.getText().toString();
		if (strPwd.isEmpty()) {
			Toast.makeText(mContext, "Please input password",
					Toast.LENGTH_SHORT).show();
			m_etPwd.requestFocus();
			return;
		}

		String strPwdConfirm = m_etPwdConfirm.getText().toString();
		if (!strPwdConfirm.equals(strPwd)) {
			Toast.makeText(mContext, "Please check confirm pwd",
					Toast.LENGTH_SHORT).show();
			m_etPwdConfirm.requestFocus();
			return;
		}

		ParseUser user = new ParseUser();
		user.setUsername(strEmail/* strUserName */);
		user.setPassword(strPwd);
		user.setEmail(strEmail);
		user.add("name", strEmail); // user.add("name", strUserName);

		user.signUpInBackground(new SignUpCallback() {
			public void done(ParseException e) {
				if (e == null) {
					// Hooray! Let them use the app now.
					Toast.makeText(mContext, "Signup Successed!",
							Toast.LENGTH_SHORT).show();
					GlobalValues.m_stUserName = strEmail;
					GlobalValues.m_stPassword = strPwd;

					AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
							mContext);

					dlgAlert.setMessage(getString(R.string.singedup_email));
					dlgAlert.setTitle("Braci");
					dlgAlert.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									if (m_nFrom == FROM_LOGIN) {
										setResult(RESULT_OK);
										finish();
									} else {
										goToLoginActivity();
									}
								}
							});
					dlgAlert.setCancelable(true);
					dlgAlert.create().show();
				} else {
					// Sign up didn't succeed. Look at the ParseException
					// to figure out what went wrong
					AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
							mContext);

					dlgAlert.setMessage(e.getMessage());
					dlgAlert.setTitle("Braci");
					dlgAlert.setPositiveButton("OK", null);
					dlgAlert.setCancelable(true);
					dlgAlert.create().show();
				}
			}
		});
	}
}
