package com.ugs.braci.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.MainApplication;
import com.ugs.braci.R;

public class ResetPwdActivity extends Activity implements OnClickListener {

	public static final boolean REQUIRED_EMAIL_VERIFIED = true;

	private final String TAG = "_ResetPwdActivity";

	private EditText m_etEmail;

	private Context mContext;

	private SharedPreferencesMgr pPhoneDb;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reset_password);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}

		pPhoneDb = ((MainApplication) getApplication())
				.getSharedPreferencesMgrPoint();
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goBack();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		m_etEmail = (EditText) findViewById(R.id.et_email);
		
		ImageView _iv = (ImageView) findViewById(R.id.iv_btn_recover);
		 _iv.setOnClickListener(this);
	}

	private void scaleView() {

		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void goBack() {
		setResult(RESULT_CANCELED);
		finish();
		overridePendingTransition(R.anim.from_bottom, R.anim.hold);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_btn_recover:
			onResetPwd();
			break;

		default:
			break;
		}
	}

	private void onResetPwd() {
		final String strEmail = m_etEmail.getText().toString();
		if (strEmail.isEmpty() || !strEmail.contains("@")) {
			Toast.makeText(mContext, "Please input valid E-mail address", Toast.LENGTH_SHORT)
					.show();
			m_etEmail.requestFocus();
			return;
		}

		AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);

		dlgAlert.setMessage("Do you really want to reset this email?");
		dlgAlert.setTitle("Braci");
		dlgAlert.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						ParseUser.requestPasswordResetInBackground(strEmail,
								new RequestPasswordResetCallback() {
									@Override
									public void done(ParseException e) {
										if (e == null) {
											AlertDialog.Builder w_dlgAlert = new AlertDialog.Builder(
													ResetPwdActivity.this);

											w_dlgAlert
													.setMessage("Please check reset email in " + strEmail);
											w_dlgAlert.setTitle("Braci");
											w_dlgAlert
													.setPositiveButton(
															"OK",
															new DialogInterface.OnClickListener() {

																@Override
																public void onClick(
																		DialogInterface dialog,
																		int which) {
																	setResult(RESULT_OK);
																	finish();
																}
															});
											w_dlgAlert.setCancelable(true);
											w_dlgAlert.create().show();
										} else {
											// Sign up didn't succeed. Look at
											// the
											// ParseException
											// to figure out what went wrong
											AlertDialog.Builder dlgAlert = new AlertDialog.Builder(
													mContext);

											dlgAlert.setMessage(e.getMessage());
											dlgAlert.setTitle("Braci");
											dlgAlert.setPositiveButton("OK",
													null);
											dlgAlert.setCancelable(true);
											dlgAlert.create().show();
										}
									}
								});
					}
				});
		dlgAlert.setNegativeButton("No", null);
		dlgAlert.setCancelable(true);
		dlgAlert.create().show();

	}
}
