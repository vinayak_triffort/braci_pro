package com.ugs.braci.setup;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.R;
import com.ugs.braci.adapter.UIListAdapter;
import com.ugs.info.UIListItemInfo;

public class BellmanFragment extends Fragment implements OnClickListener {

	private SetupActivity mActivity;

	private ListView m_listItems;
	private UIListAdapter m_adapterItems;

	private TextView m_tvDescription;

	private Fragment m_oldFragment;
	private String m_strOldTitle;

	private final int ITEM_BELLMAN_ALARMCLOCK = 0;
	private final int ITEM_BELLMAN_SMOKEALARM = 1;
	private final int ITEM_BELLMAN_DOORBELL = 2;
	private final int ITEM_BELLMAN_TELEPHONE = 3;

	private int[] _soundTypes = { PRJCONST.REC_SOUND_TYPE_ALARMCLOCK,
			PRJCONST.REC_SOUND_TYPE_SMOKEALARM,
			PRJCONST.REC_SOUND_TYPE_DOORBELL,
			PRJCONST.REC_SOUND_TYPE_LINEPHONE,};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_general, container,
				false);

		mActivity = (SetupActivity) getActivity();

		m_oldFragment = mActivity.m_curFragment;
		m_strOldTitle = mActivity.m_tvTitle.getText().toString();

		mActivity.m_curFragment = BellmanFragment.this;
		mActivity.m_tvTitle.setText(mActivity
				.getString(R.string.setupdeaf_bellman_title));

		mActivity.m_nDeafSndType = PRJCONST.DEAF_BELLMAN;

		if (m_oldFragment == null) {
			PRJFUNC.hideFragment(mActivity, mActivity.m_setupFragment);
		} else {
			PRJFUNC.hideFragment(mActivity, m_oldFragment);
		}

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		return v;
	}

	@Override
	public void onDestroyView() {
		if (mActivity.m_curFragment == BellmanFragment.this) {
			mActivity.m_curFragment = m_oldFragment;
			mActivity.m_tvTitle.setText(m_strOldTitle);

			if (mActivity.m_curFragment == null) {
				PRJFUNC.showFragment(mActivity, mActivity.m_setupFragment);
			} else {
				PRJFUNC.showFragment(mActivity, m_oldFragment);
			}
		}

		super.onDestroyView();
	}

	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		itemInfo = new UIListItemInfo(R.drawable.ic_bellman_alarmclock,
				mActivity.getString(R.string.bellman_alarmclock), null, true);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_bellman_smokealarm,
				mActivity.getString(R.string.bellman_smokealarm), null, true);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_bellman_doorbell,
				mActivity.getString(R.string.bellman_doorbell), null, true);
		m_adapterItems.add(itemInfo);
		
		itemInfo = new UIListItemInfo(R.drawable.ic_bellman_telephone,
				mActivity.getString(R.string.bellman_telephone), null, true);
		m_adapterItems.add(itemInfo);		

		// set refernces
		for (int i = 0; i < _soundTypes.length; i++) {
			m_adapterItems.getItem(i).m_extraData = (Integer) _soundTypes[i];
		}

		// set check box
		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mActivity);
		if (phoneDb.getCurDeafSndType(mActivity.m_nProfileModeForRecording) == mActivity.m_nDeafSndType) {
			int[] nSelSndIndices = phoneDb
					.loadDeafSelectedSndIndices(mActivity.m_nProfileModeForRecording);
			for (int i = 0; i < m_adapterItems.getCount(); i++) {
				itemInfo = m_adapterItems.getItem(i);
				if (nSelSndIndices[(Integer) itemInfo.m_extraData] >= 0) {
					itemInfo.setChecked(true);
				}
			}
			m_adapterItems.notifyDataSetChanged();
		}
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
		m_tvDescription.setText(mActivity
				.getString(R.string.setupdeaf_select_desc));

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UIListAdapter(mActivity,
				R.layout.list_item_title_only, new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		View _v = v.findViewById(R.id.frm_btn_ok);
		_v.setVisibility(View.VISIBLE);

		ImageView _iv = (ImageView) v.findViewById(R.id.iv_ok);
		_iv.setOnClickListener(this);

		refreshList();
	}

	UIListAdapter.Callback m_adapterCallback = new UIListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			UIListItemInfo itemInfo = m_adapterItems.getItem(p_nPosition);
			itemInfo.m_bChecked = !itemInfo.m_bChecked;
			m_adapterItems.notifyDataSetChanged();

			switch (p_nPosition) {
			case ITEM_BELLMAN_ALARMCLOCK:

				break;
			case ITEM_BELLMAN_SMOKEALARM:

				break;
			case ITEM_BELLMAN_DOORBELL:

				break;
			case ITEM_BELLMAN_TELEPHONE:

				break;
			default:
				break;
			}
		}
	};

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_ok:
			onOk();
			break;

		default:
			break;
		}

	}

	private void onOk() {
		ArrayList<Integer> selectedTypes = new ArrayList<Integer>();
		for (int i = 0; i < m_adapterItems.getCount(); i++) {
			UIListItemInfo itemInfo = m_adapterItems.getItem(i);
			if (itemInfo.m_bChecked) {
				selectedTypes.add((Integer) itemInfo.m_extraData);
			}
		}

		if (selectedTypes.size() == 0) {
			Toast.makeText(mActivity,
					mActivity.getString(R.string.setup_deaf_no_selected),
					Toast.LENGTH_SHORT).show();
			return;
		}
		
		mActivity.m_sndTypesToSaveDeaf = selectedTypes;

		mActivity.askSaveDeaf();
	}
}
