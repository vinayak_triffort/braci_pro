package com.ugs.braci.setup;

import java.io.File;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.uc.popup.DlgSetupProfile;
import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.R;
import com.ugs.braci.profile.ProfileModeActivity;

public class SetupActivity extends FragmentActivity implements OnClickListener {

	private Context mContext;

	public static final String AUTO_OPENED = "auto_opened";
	public static final int ACTIVITY_RECORD_DONE = 10;
	public static final int ACTIVITY_MATCHING_FAILED = 11;
	public static final int ACTIVITY_RECORD_FAILED = 12;
	private static final int DLG_SELECT_MODE = 1;

	public int m_nDeafSndType = -1;
	public ArrayList<Integer> m_sndTypesToSaveDeaf = null;

	public boolean m_bAutoOpened = false;
	
	public int m_nRepeatedListenCnt = 0;

	public int m_nProfileModeForRecording = PRJCONST.PROFILE_MODE_HOME;

	public SetupFragment m_setupFragment;
	public Fragment m_curFragment;

	public TextView m_tvTitle;

	public static boolean[] m_bSoundsRecordable = {
	/* Doorbell */true,
	/* BackDoorbell */true,
	/* SmokeAlarm */false,
	/* Telephone */true,
	/* Intercom */true,
	/* Carbon */false,
	/* AlarmClock */true,
	/* Theft */true,
	/* Microwave */true};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);
		ActivityTask.INSTANCE.add(this);

		mContext = this;

		m_bAutoOpened = getIntent().getBooleanExtra(AUTO_OPENED, false);

		boolean bHomeFirstRecord = isFirstRecord(PRJCONST.PROFILE_MODE_HOME);
		boolean bOfficeFirstRecord = isFirstRecord(PRJCONST.PROFILE_MODE_OFFICE);

		if (bHomeFirstRecord && bOfficeFirstRecord) {
			m_nProfileModeForRecording = PRJCONST.PROFILE_MODE_HOME;
		} else {
			m_nProfileModeForRecording = GlobalValues.m_nProfileIndoorMode;
		}

		updateLCD();

		m_setupFragment = new SetupFragment();
		
		PRJFUNC.addFragment(SetupActivity.this, m_setupFragment);
		

		if (!m_bAutoOpened) {
			if (!bHomeFirstRecord || !bOfficeFirstRecord) {
				Intent intent = new Intent(SetupActivity.this,
						DlgSetupProfile.class);
				intent.putExtra(DlgSetupProfile.PARAM_HOME_EXIST,
						!bHomeFirstRecord);
				intent.putExtra(DlgSetupProfile.PARAM_OFFICE_EXIST,
						!bOfficeFirstRecord);
				startActivityForResult(intent, DLG_SELECT_MODE);
			}
		}
		m_curFragment = null;
	}

	@Override
	protected void onDestroy() {
		PRJFUNC.closeProgress(mContext);

		ActivityTask.INSTANCE.remove(this);
		
		if (m_curFragment != null) {
			PRJFUNC.removeFragment(SetupActivity.this, m_curFragment);
		}

		releaseValues();

		super.onDestroy();
	}

	private void releaseValues() {
		m_setupFragment = null;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goBack();
			return false;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent intent) {

		if (requestCode == ACTIVITY_RECORD_DONE) {
			if (resultCode == RESULT_OK) {
				final int nResultType = intent.getIntExtra(
						RecordDoneActivity.RESULT_PARAM,
						RecordDoneActivity.RESULT_DONE);
				final int nRecordType = intent.getIntExtra(
						RecordDoneActivity.RECORD_TYPE, 0);
				final int nDeafParam1 = intent.getIntExtra(
						RecordDoneActivity.DEAF_PARAM1, -1);
				final int nDeafParam2 = intent.getIntExtra(
						RecordDoneActivity.DEAF_PARAM2, 0);
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if (nDeafParam1 < 0) {
							PRJFUNC.removeFragment(SetupActivity.this, m_curFragment);
							
							if (nResultType == RecordDoneActivity.RESULT_DONE)
								onNextRecording(false, -1);
							else
								onNextRecording(false, nRecordType);
						} else {
							if (nResultType == RecordDoneActivity.RESULT_DONE)
								continueSaveDeafs(nDeafParam1, nDeafParam2,
										false);
							else
								continueSaveDeafs(nDeafParam1, nDeafParam2,
										true);
						}
					}
				}, 10);

			}
		} else if (requestCode == ACTIVITY_MATCHING_FAILED) {
			if (resultCode == RESULT_OK) {
				final int nRecordType = intent.getIntExtra(
						MatchingFailActivity.RECORD_TYPE, 0);
				final int nDeafParam1 = intent.getIntExtra(
						MatchingFailActivity.DEAF_PARAM1, -1);
				final int nDeafParam2 = intent.getIntExtra(
						MatchingFailActivity.DEAF_PARAM2, 0);
				new Handler().postDelayed(new Runnable() {
					@Override
					public void run() {
						if (nDeafParam1 < 0) {
							PRJFUNC.removeFragment(SetupActivity.this, m_curFragment);
							onNextRecording(false, nRecordType);
						} else {
							continueSaveDeafs(nDeafParam1, nDeafParam2, true);
						}
					}
				}, 10);

			}
		} else if (requestCode == DLG_SELECT_MODE) {
			if (resultCode == RESULT_OK) {
				m_nProfileModeForRecording = intent.getIntExtra(
						DlgSetupProfile.RESULT_STR,
						GlobalValues.m_nProfileIndoorMode);
			}

			if (m_nProfileModeForRecording == PRJCONST.PROFILE_MODE_HOME) {
				m_tvTitle.setText(getString(R.string.setup_title)+" " 
						+ getString(R.string.word_home) );
			} else if (m_nProfileModeForRecording == PRJCONST.PROFILE_MODE_OFFICE) {
				m_tvTitle.setText(getString(R.string.setup_title) + " "
						+ getString(R.string.word_office));
			}

			m_setupFragment.refreshCheckBoxes();
		}

		super.onActivityResult(requestCode, resultCode, intent);
	}

	private void updateLCD() {
		m_tvTitle = (TextView) findViewById(R.id.tv_title);
		// PRJFUNC.setTextViewFont(mContext, m_tvTitle,
		// PRJCONST.FONT_AuctionGothicBold);

		if (m_nProfileModeForRecording == PRJCONST.PROFILE_MODE_HOME) {
			m_tvTitle.setText(getString(R.string.setup_title) + " "
					+ getString(R.string.word_home));
		} else if (m_nProfileModeForRecording == PRJCONST.PROFILE_MODE_OFFICE) {
			m_tvTitle.setText(getString(R.string.setup_title) + " "
					+ getString(R.string.word_office));
		}

		ImageView _iv = (ImageView) findViewById(R.id.iv_back);
		_iv.setOnClickListener(this);

		_iv = (ImageView) findViewById(R.id.iv_help_menu);
		_iv.setOnClickListener(this);
		_iv.setVisibility(View.GONE);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_back:
			goBack();
			break;
		case R.id.iv_help_menu:
			onHelpMenu();
			break;
		default:
			break;
		}
	}

	private void onHelpMenu() {
		// TODO Auto-generated method stub
	}

	public void goBack() {
		if (m_curFragment != null) {
			PRJFUNC.removeFragment(SetupActivity.this, m_curFragment);
		} else {
			finish();
			overridePendingTransition(R.anim.hold, R.anim.right_out);
		}
	}

	public void onRecordSound(int p_nRecordType, int p_nDeafParam) {
		if (!m_bSoundsRecordable[p_nRecordType]) {
			Toast.makeText(mContext,
					getString(R.string.notify_based_on_universal),
					Toast.LENGTH_SHORT).show();
			return;
		}
		RecordFragment fragment = new RecordFragment();
		fragment.setSoundType(p_nRecordType);
		fragment.setDeafParam(p_nDeafParam);
		PRJFUNC.addFragment(SetupActivity.this, fragment);
	}

	public void onDeafProductsSelected() {
		SetupDeafsFragment fragment = new SetupDeafsFragment();
		PRJFUNC.addFragment(SetupActivity.this, fragment);
	}

	public void onDeafSubProductsSelected(int p_nDeafType) {
		Fragment fragment = null;
		switch (p_nDeafType) {
		case SetupDeafsFragment.ITEM_DEAF_BELLMAN:
			fragment = new BellmanFragment();
			break;
		case SetupDeafsFragment.ITEM_DEAF_BYRON:
			fragment = new ByronFragment();
			break;
		case SetupDeafsFragment.ITEM_DEAF_FIREDLAND:
			fragment = new FriedLandFragment();
			break;
		case SetupDeafsFragment.ITEM_DEAF_GREENBROCK:
			fragment = new GreenBrookFragment();
			break;
		case SetupDeafsFragment.ITEM_DEAF_ECHO:
			fragment = new EchoFragment();
			break;
		case SetupDeafsFragment.ITEM_DEAF_GEEMARK:
			fragment = new GeemarkFragment();
			break;
		case SetupDeafsFragment.ITEM_DEAF_AMPLICOM:
			fragment = new AmplicomFragment();
			break;
		}

		if (fragment != null) {
			PRJFUNC.addFragment(SetupActivity.this, fragment);
		}
	}

	public void onByronTypeSelected(int p_nByronType) {
		ByronSubFragment fragment = new ByronSubFragment();
		fragment.setByronType(p_nByronType);
		PRJFUNC.addFragment(SetupActivity.this, fragment);
	}

	public void onFiredlandTypeSelected(int p_nFiredlandType) {
		FriedLandSubFragment fragment = new FriedLandSubFragment();
		fragment.setFiredlandType(p_nFiredlandType);
		PRJFUNC.addFragment(SetupActivity.this, fragment);
	}

	public void onGreenBrookTypeSelected(int p_nGreenBrookType) {
		GreenBrookSubFragment fragment = new GreenBrookSubFragment();
		// fragment.setFiredlandType(p_nGeemarkType);
		PRJFUNC.addFragment(SetupActivity.this, fragment);
	}

	public void onAmplicomTypeSelected(int p_nAmplicomType) {
		AmplicomSubFragment fragment = new AmplicomSubFragment();
		// fragment.setFiredlandType(p_nGeemarkType);
		PRJFUNC.addFragment(SetupActivity.this, fragment);
	}

	public void onGeemarkTypeSelected(int p_nGeemarkType) {
		GeemarkSubFragment fragment = new GeemarkSubFragment();
		fragment.setGeemarkType(p_nGeemarkType);
		PRJFUNC.addFragment(SetupActivity.this, fragment);
	}

	public void onEchoTypeSelected(int p_nEchoType) {
		EchoSubFragment fragment = new EchoSubFragment();
		fragment.setEchoType(p_nEchoType);
		PRJFUNC.addFragment(SetupActivity.this, fragment);
	}

	public void onNextRecording(boolean bStart, int nRepeatSndType) {
		int nTypeToRecord = nRepeatSndType;
		if (nTypeToRecord < 0) {
			nTypeToRecord = m_setupFragment.getNextRecordingType();
			m_nRepeatedListenCnt = 0;
		} else {
			m_nRepeatedListenCnt++;
		}

		if (nTypeToRecord >= 0) {
			onRecordSound(nTypeToRecord, -1);
		} else {
			if (bStart) {
				Toast.makeText(mContext,
						getString(R.string.setupdeaf_noselected_record),
						Toast.LENGTH_SHORT).show();
			} else {
				if ((GlobalValues.location_home_lat == PRJCONST.INVALID_LOCATION_VALUE)
						&& (m_nProfileModeForRecording == PRJCONST.PROFILE_MODE_HOME)) {
					goToProfileModeActivity();
					return;
				} else if ((GlobalValues.location_office_lat == PRJCONST.INVALID_LOCATION_VALUE)
						&& (m_nProfileModeForRecording == PRJCONST.PROFILE_MODE_OFFICE)) {
					goToProfileModeActivity();
					return;
				} else {
					Toast.makeText(mContext,
							getString(R.string.setup_record_alldone),
							Toast.LENGTH_SHORT).show();

					finish();
				}
			}
		}
	}

	private void goToProfileModeActivity() {
		Intent it = new Intent(SetupActivity.this, ProfileModeActivity.class);
		it.putExtra(ProfileModeActivity.AUTO_OPENED, true);
		if (m_bAutoOpened) {
			it.putExtra(ProfileModeActivity.SETUP_PROFILE_MODE, -1);
		} else {
			it.putExtra(ProfileModeActivity.SETUP_PROFILE_MODE,
					m_nProfileModeForRecording);
		}
		startActivity(it);
		finish();
	}

	public boolean isExistSound(int p_nRecSndType) {
		String strFilePath = PRJFUNC.getRecordDir(p_nRecSndType,
				m_nProfileModeForRecording) + "/Detect.dat";
		File file = new File(strFilePath);
		if (file.exists())
			return true;
		return false;
	}

	public boolean isFirstRecord(int p_nProfileMode) {
		File recordRootDir = new File(PRJFUNC.getRecordRootDir(p_nProfileMode));
		return !recordRootDir.exists();
	}

	protected void continueSaveDeafs(int nDeafParam1, int nDeafParam2,
			boolean bRepeat) {
		if (!bRepeat) {
			saveDeafSounds(nDeafParam1, nDeafParam2);
		} else {
			int nCurIdx = nDeafParam1;
			int nSndType = m_sndTypesToSaveDeaf.get(nCurIdx);
			m_nRepeatedListenCnt++;
			onRecordSound(nSndType, nCurIdx);
		}
	}

	public void askSaveDeaf() {
		final File recDir = new File(
				PRJFUNC.getRecordRootDir(m_nProfileModeForRecording));

		if (recDir.exists()) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
			alertDialog.setTitle(R.string.setup_title);
			alertDialog.setMessage(R.string.setup_delete_record_sound);
			alertDialog.setPositiveButton(R.string.word_yes,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							PRJFUNC.deleteFile(recDir, false);

							saveDeafSounds(-1, -1);
						}
					});
			alertDialog.setNegativeButton(R.string.word_no, null);
			alertDialog.show();
		} else {
			saveDeafSounds(-1, -1);
		}
	}

	private void saveDeafSounds(int p_nCurIdx, int p_nSndIdx) {
		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mContext);

		if (p_nCurIdx >= 0) {
			phoneDb.saveDeafSelectedSndIndex(m_nProfileModeForRecording,
					m_sndTypesToSaveDeaf.get(p_nCurIdx), p_nSndIdx);
		}

		p_nCurIdx++;
		while (p_nCurIdx < m_sndTypesToSaveDeaf.size()) {

			int nSndType = m_sndTypesToSaveDeaf.get(p_nCurIdx);
			if (!PRJFUNC.haveMultiData(m_nDeafSndType, nSndType)) {
				phoneDb.saveDeafSelectedSndIndex(m_nProfileModeForRecording,
						nSndType, 0);
			} else {
				m_nRepeatedListenCnt = 0;
				onRecordSound(nSndType, p_nCurIdx);
				return;
			}

			p_nCurIdx++;
		}

		phoneDb.setCurDeafSndType(m_nProfileModeForRecording, m_nDeafSndType);
		if (m_nProfileModeForRecording == GlobalValues.m_nProfileIndoorMode) {
			PRJFUNC.initRecordedData(mContext,
					GlobalValues.m_nProfileIndoorMode);
		}
		goBack();
	}

}
