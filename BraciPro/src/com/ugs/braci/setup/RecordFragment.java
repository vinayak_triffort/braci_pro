package com.ugs.braci.setup;

import java.io.File;
import java.text.BreakIterator;
import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.SharedPreferencesMgr;
import com.ugs.braci.GlobalValues;
import com.ugs.braci.R;
import com.ugs.braci.engine.DetectingData;
import com.ugs.braci.engine.SoundEngine;
import com.ugs.lib.BuketProgressBar;

public class RecordFragment extends Fragment implements OnClickListener {

	private SetupActivity mActivity;

	private Fragment m_oldFragment;
	private String m_strOldTitle;

	private TextView m_tvOrder;
	private TextView m_tvInstruction;
	private TextView m_tvDesc1;

	private ImageView m_ivRecordStart;
	private BuketProgressBar m_buketProgressBar;
	private TextView m_tvProgress;
	private ImageView m_ivRecordingMotion;;

	private View m_frmCenterRecording;

	private int m_nSoundType;

	private boolean m_bRecording = false;

	private DetectingData m_curDetectingData;

	private int m_nDeafParam = -1;

	private enum ListeningType {
		Recording, Matching,
	};

	ListeningType m_listenType = ListeningType.Recording;
	private int m_nMatchedIdx = -1;

	public void setDeafParam(int p_nDeafParam) {
		m_nDeafParam = p_nDeafParam;
	}

	public void setSoundType(int p_nSoundType) {
		m_nSoundType = p_nSoundType;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_record, container,
				false);

		mActivity = (SetupActivity) getActivity();

		if (m_nDeafParam >= 0
				|| m_nSoundType == PRJCONST.REC_SOUND_TYPE_THEFTALARM) {
			m_listenType = ListeningType.Matching;
		}

		m_oldFragment = mActivity.m_curFragment;
		m_strOldTitle = mActivity.m_tvTitle.getText().toString();

		mActivity.m_curFragment = RecordFragment.this;

		String strTitle = getSoundTitle();
		mActivity.m_tvTitle.setText(strTitle);

		if (m_oldFragment == null) {
			PRJFUNC.hideFragment(mActivity, mActivity.m_setupFragment);
		} else {
			PRJFUNC.hideFragment(mActivity, m_oldFragment);
		}

		if (GlobalValues._soundEngine == null) {
			GlobalValues._soundEngine = new SoundEngine();
		}

		m_curDetectingData = new DetectingData();
		PRJFUNC.m_curRecordingSoundType = m_nSoundType;
		m_curDetectingData.setSoundType(PRJFUNC.m_curRecordingSoundType);

		updateLCD(v);

		updateCurScreen();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		GlobalValues._soundEngine.setNotDetecting(true);

		if (m_listenType == ListeningType.Matching) {
			if (m_nDeafParam >= 0) {
				PRJFUNC.loadDetectDataToMatch(mActivity,
						PRJFUNC.getDeafSoundDir(mActivity.m_nDeafSndType,
								m_nSoundType),
						SoundEngine.MAX_DETECT_EXTRA_SOUNDS_FRAME_CNT[7]);
			} else {
				GlobalValues._matchingData = GlobalValues._thiefDetectData;
				PRJFUNC.g_strOldMatchingPath = null;
			}
		}

		return v;
	}

	@Override
	public void onDestroyView() {
		if (mActivity.m_curFragment == RecordFragment.this) {
			mActivity.m_curFragment = m_oldFragment;
			mActivity.m_tvTitle.setText(m_strOldTitle);

			if (mActivity.m_curFragment == null) {
				PRJFUNC.showFragment(mActivity, mActivity.m_setupFragment);
			} else {
				PRJFUNC.showFragment(mActivity, m_oldFragment);
			}
		}

		GlobalValues._soundEngine.RecordStop(true);
		GlobalValues._soundEngine.setMatching(false, null);
		GlobalValues._soundEngine.setNotDetecting(false);

		super.onDestroyView();
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvOrder = (TextView) v.findViewById(R.id.tv_order);
		m_tvInstruction = (TextView) v.findViewById(R.id.tv_instruction);
		m_tvDesc1 = (TextView) v.findViewById(R.id.tv_instruction3);

		m_buketProgressBar = (BuketProgressBar) v
				.findViewById(R.id.progress_recording);
		m_buketProgressBar.setIndeterminate(false);
		if (m_listenType == ListeningType.Recording) {
			m_buketProgressBar
					.setMax(SoundEngine.RECORD_SOUND_MAX_FRAMES[PRJFUNC.m_curRecordingSoundType]);
		} else {
			m_buketProgressBar.setMax(SoundEngine.MATCH_SOUND_MAX_FRAMES);
		}
		m_buketProgressBar.setValue(0);

		m_tvProgress = (TextView) v.findViewById(R.id.tv_progress);
		m_tvProgress.setText("0%");

		m_frmCenterRecording = v.findViewById(R.id.frm_center_recording);
		m_ivRecordingMotion = (ImageView) v
				.findViewById(R.id.iv_recording_motion);

		m_ivRecordStart = (ImageView) v.findViewById(R.id.iv_record_start);
		m_ivRecordStart.setOnClickListener(this);
	}

	private int[] nStringTitleIds = { R.string.setup_doorbell_title,
			R.string.setup_backdoorbell_title, R.string.setup_smokealarm_title,
			R.string.setup_landline_title, R.string.setup_intercom_title,
			R.string.setup_carbon_title, R.string.setup_alarmclock_title,
			R.string.setup_thief_title, R.string.setup_microwave_title, };

	private String getSoundTitle() {
		return mActivity.getString(nStringTitleIds[m_nSoundType]);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_record_start:
			onRecordStart();
			break;

		default:
			break;
		}

	}

	private void onRecordStart() {
		if (m_bRecording)
			return;

		m_nMatchedIdx = -1;

		if (m_listenType == ListeningType.Recording) {
			int nRecordIdx = 0;
			String strRecordFileName = SoundEngine.RECORD_FILE
					+ String.valueOf(nRecordIdx) + SoundEngine.RECORD_FILE_EXT;
			m_bRecording = GlobalValues._soundEngine.RecordStart(
					strRecordFileName,
					SoundEngine.RECORD_STARTING_THRESHOLDS[m_nSoundType],
					m_handler);
		} else if (m_listenType == ListeningType.Matching) {
			GlobalValues._soundEngine.setMatching(true, m_handler);
			m_bRecording = true;
		}

		if (m_bRecording) {
			updateCurScreen();
		} else {
			Toast.makeText(mActivity,
					getResources().getString(R.string.toast11),
					Toast.LENGTH_SHORT).show();
			updateCurScreen();
		}
	}

	private void updateCurScreen() {
		if (m_bRecording) {
			m_tvOrder.setVisibility(View.VISIBLE);

			m_frmCenterRecording.setVisibility(View.VISIBLE);
			m_ivRecordStart.setVisibility(View.INVISIBLE);

			m_ivRecordingMotion.setVisibility(View.VISIBLE);
			m_ivRecordingMotion.setBackgroundResource(R.drawable.ic_none);

			m_tvInstruction.setVisibility(View.GONE);
			m_tvDesc1.setVisibility(View.GONE);

			m_buketProgressBar.setValue(0);

		} else {
			if (PRJFUNC.m_curRecordingSoundType == PRJCONST.REC_SOUND_TYPE_DOORBELL
					|| PRJFUNC.m_curRecordingSoundType == PRJCONST.REC_SOUND_TYPE_BACKDOORBELL) {
				m_tvOrder.setText(getString(R.string.rec_order_doorbell));
				m_tvInstruction
						.setText(getString(R.string.rec_instruction_doorbell));
			} else if (PRJFUNC.m_curRecordingSoundType == PRJCONST.REC_SOUND_TYPE_SMOKEALARM) {
				m_tvOrder.setText(getString(R.string.rec_order_smokealarm));
				m_tvInstruction
						.setText(getString(R.string.rec_instruction_smokealarm));
			} else if (PRJFUNC.m_curRecordingSoundType == PRJCONST.REC_SOUND_TYPE_LINEPHONE) {
				m_tvOrder.setText(getString(R.string.rec_order_landline));
				m_tvInstruction
						.setText(getString(R.string.rec_instruction_landline));
			} else if (PRJFUNC.m_curRecordingSoundType == PRJCONST.REC_SOUND_TYPE_INTERCOM) {
				m_tvOrder.setText(getString(R.string.rec_order_intercom));
				m_tvInstruction
						.setText(getString(R.string.rec_instruction_intercom));
			} else if (PRJFUNC.m_curRecordingSoundType == PRJCONST.REC_SOUND_TYPE_CO2) {
				m_tvOrder.setText(getString(R.string.rec_order_carbon));
				m_tvInstruction
						.setText(getString(R.string.rec_instruction_carbon));
			} else if (PRJFUNC.m_curRecordingSoundType == PRJCONST.REC_SOUND_TYPE_ALARMCLOCK) {
				m_tvOrder.setText(getString(R.string.rec_order_alarmclock));
				m_tvInstruction
						.setText(getString(R.string.rec_instruction_alarmclock));
			} else if (PRJFUNC.m_curRecordingSoundType == PRJCONST.REC_SOUND_TYPE_THEFTALARM) {
				m_tvOrder.setText(getString(R.string.rec_order_thief));
				m_tvInstruction
						.setText(getString(R.string.rec_instruction_thief));
			} else if (PRJFUNC.m_curRecordingSoundType == PRJCONST.REC_SOUND_TYPE_MICROWAVE) {
				m_tvOrder.setText(getString(R.string.rec_order_microwave));
				m_tvInstruction
						.setText(getString(R.string.rec_instruction_microwave));
			}

			m_tvOrder.setVisibility(View.INVISIBLE);

			m_ivRecordingMotion.setVisibility(View.INVISIBLE);
			m_frmCenterRecording.setVisibility(View.INVISIBLE);
			m_ivRecordStart.setVisibility(View.VISIBLE);

			m_buketProgressBar.setValue(0);
		}
	}

	Handler m_handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case PRJCONST.RECORDING_PROGRESS: {
				int val = (Integer) msg.obj;
				if (val >= m_buketProgressBar.getMax())
					val = m_buketProgressBar.getMax();
				m_buketProgressBar.setValue(val);
				int nProgress = val * 100 / m_buketProgressBar.getMax();
				m_tvProgress.setText(nProgress + "%");
				int nMotion = nProgress / 5;
				if (nMotion % 4 == 0) {
					m_ivRecordingMotion
							.setBackgroundResource(R.drawable.ic_none);
				} else if (nMotion % 4 == 1) {
					m_ivRecordingMotion
							.setBackgroundResource(R.drawable.bg_record_motion_1);
				} else if (nMotion % 4 == 2) {
					m_ivRecordingMotion
							.setBackgroundResource(R.drawable.bg_record_motion_2);
				} else if (nMotion % 4 == 3) {
					m_ivRecordingMotion
							.setBackgroundResource(R.drawable.bg_record_motion_3);
				}
			}
				break;
			case PRJCONST.RECORD_STOPPED:
				onRecordStop();
				break;

			case GlobalValues.COMMAND_MATCHED_PRODUCT:
				m_nMatchedIdx = (Integer) msg.obj;
				onRecordStop();
				break;
			default:
				break;
			}
		}
	};

	private void onRecordStop() {
		if (m_curDetectingData == null)
			return;

		if (m_listenType == ListeningType.Matching) {
			// for Deaf sound matching
			GlobalValues._soundEngine.setMatching(false, null);
			onAllRecorded();
			return;
		}

		int nRecordIdx = 0; // m_nRecordStep - 1;
		boolean bRecord = m_curDetectingData.addProcessData(DetectingData
				.getFilePath(SoundEngine.RECORD_DIR, nRecordIdx));
		m_bRecording = false;

		if (bRecord) {
			if (m_curDetectingData.getRecordedCnt() == SoundEngine.MAX_RECORD_TIMES) {
				if (m_curDetectingData.ExtractDetectingData()) {

					m_curDetectingData.SaveDetectData(PRJFUNC.getRecordDir(
							PRJFUNC.m_curRecordingSoundType,
							mActivity.m_nProfileModeForRecording), "Detect"
							+ /* detectingDataList.size() + */".dat");

					if (mActivity.m_nProfileModeForRecording == GlobalValues.m_nProfileIndoorMode) {
						if (GlobalValues.recordedDetectData == null)
							GlobalValues.recordedDetectData = new Object[PRJCONST.REC_SOUND_TYPE_CNT];

						if (GlobalValues.recordedDetectData[PRJFUNC.m_curRecordingSoundType] == null)
							GlobalValues.recordedDetectData[PRJFUNC.m_curRecordingSoundType] = new ArrayList<DetectingData>();

						ArrayList<DetectingData> detectingDataList = (ArrayList<DetectingData>) GlobalValues.recordedDetectData[PRJFUNC.m_curRecordingSoundType];
						detectingDataList.clear();

						detectingDataList.add(m_curDetectingData);
					}

					Toast.makeText(mActivity,
							getResources().getString(R.string.toast8),
							Toast.LENGTH_LONG).show();
					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							onAllRecorded();
						}
					}, 1000);
				} else {
					Toast.makeText(mActivity,
							getResources().getString(R.string.toast9),
							Toast.LENGTH_LONG).show();
					reset();

					m_curDetectingData.resetProcessData();
					// m_nRecordStep = 1;
					updateCurScreen();
				}
			} else {
				Toast.makeText(mActivity,
						getResources().getString(R.string.toast10),
						Toast.LENGTH_SHORT).show();
				// m_nRecordStep++;
				updateCurScreen();
			}
		} else {
			Intent intent = new Intent(mActivity, RecordDoneActivity.class);
			intent.putExtra(RecordDoneActivity.RECORD_SUCCESS, false);
			intent.putExtra(RecordDoneActivity.RECORD_TYPE, m_nSoundType);
			intent.putExtra(RecordDoneActivity.DEAF_PARAM1, m_nDeafParam);
			intent.putExtra(RecordDoneActivity.DEAF_PARAM2, m_nMatchedIdx);
			mActivity.startActivityForResult(intent,
					SetupActivity.ACTIVITY_RECORD_DONE);
		}
	}

	protected void onAllRecorded() {

		reset();

		SharedPreferencesMgr phoneDb = new SharedPreferencesMgr(mActivity);
		if (m_listenType == ListeningType.Recording) {
			if (GlobalValues._bRecordedSoundsDetectable == null) {
				phoneDb.loadRecordedDetectableInfo();
			}

			GlobalValues._bRecordedSoundsDetectable[m_nSoundType] = true;
			phoneDb.saveRecordedDetectableInfo(m_nSoundType);
		}

		mActivity.goBack();

		if (m_listenType == ListeningType.Matching
				&& mActivity.m_nRepeatedListenCnt >= 2) {
			if (m_nMatchedIdx < 0)
				m_nMatchedIdx = 0;
		}

		if (m_listenType == ListeningType.Matching && m_nMatchedIdx < 0) {
			Intent intent = new Intent(mActivity, MatchingFailActivity.class);
			intent.putExtra(MatchingFailActivity.RECORD_TYPE, m_nSoundType);
			intent.putExtra(MatchingFailActivity.DEAF_PARAM1, m_nDeafParam);
			intent.putExtra(MatchingFailActivity.DEAF_PARAM2, m_nMatchedIdx);
			mActivity.startActivityForResult(intent,
					SetupActivity.ACTIVITY_MATCHING_FAILED);
		} else {
			if (m_listenType == ListeningType.Matching && m_nDeafParam < 0) {
				// theif alarm
				phoneDb.saveTheifAlarmIndex(
						mActivity.m_nProfileModeForRecording, m_nMatchedIdx);
				if (mActivity.m_nProfileModeForRecording == GlobalValues.m_nProfileIndoorMode) {
					SoundEngine.THIEF_ALARM_IDX = m_nMatchedIdx;
				}
			}
			Intent intent = new Intent(mActivity, RecordDoneActivity.class);
			intent.putExtra(RecordDoneActivity.RECORD_TYPE, m_nSoundType);
			intent.putExtra(RecordDoneActivity.DEAF_PARAM1, m_nDeafParam);
			intent.putExtra(RecordDoneActivity.DEAF_PARAM2, m_nMatchedIdx);
			mActivity.startActivityForResult(intent,
					SetupActivity.ACTIVITY_RECORD_DONE);
		}

	}

	private void reset() {
		for (int idx = 0; idx < SoundEngine.MAX_RECORD_TIMES; idx++) {
			String strRecordFileName = SoundEngine.RECORD_FILE
					+ String.valueOf(idx) + SoundEngine.RECORD_FILE_EXT;
			File file = new File(SoundEngine.RECORD_DIR, strRecordFileName);
			file.delete();

			file = new File(SoundEngine.RECORD_DIR, strRecordFileName + "_");
			file.delete();
		}
	}

}
