package com.ugs.braci.history;

import java.util.ArrayList;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJFUNC;
import com.uc.sqlite.BraciDetectedDb;
import com.uc.sqlite.SQLDBHelper;
import com.ugs.braci.R;
import com.ugs.braci.engine.DetectingData;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

public class HistoryActivity extends FragmentActivity implements OnClickListener {
	
	private Context mContext;
	
	public HistoryActivityFragment m_settingsFragment;
	public Fragment m_curFragment;
	public static int count=0;
	public TextView m_tvTitle;
	
	BraciDetectedDb historyData;
	SQLiteDatabase db;
	Cursor  cursorHistory;
	
	public static ArrayList<String> detectedDate;
	public static ArrayList<String> detectedSound;
	
	public static  String date, time, soundTypeDet, record;
	
	public static ArrayList<String> histRecord;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);
		ActivityTask.INSTANCE.add(this);

		mContext = this;
		
		

		updateLCD();
		
		m_tvTitle.setText(getString(R.string.menu_history));
		
		m_settingsFragment = new HistoryActivityFragment();
		PRJFUNC.addFragment(HistoryActivity.this, m_settingsFragment);
		
		detectedDate = new ArrayList<String>();
		detectedSound = new ArrayList<String>();
		
		
		 historyData = new BraciDetectedDb(HistoryActivity.this);
		 db = historyData.getReadableDatabase();
		 cursorHistory = db.query(BraciDetectedDb.dbTable, null, null, null, null, null, null);
		 count = cursorHistory.getCount();
		while(cursorHistory.moveToNext()){
			detectedDate.add(cursorHistory.getString(0));
			detectedSound.add(cursorHistory.getString(1));
			
		 }
//		initValues();
		cursorHistory.close();
		db.close();
		
		
	}

//	private void initValues() {
//		// TODO Auto-generated method stub
////		histRecord = new ArrayList<String>();
////		if(count != 0){
//////			date = detectedDay+"\n"+detectedMonth;
////			
////			soundTypeDet = detectedSound+" "+"at";
////			
////			histRecord.add(record);
////		}
//	}

	private void updateLCD() {
		// TODO Auto-generated method stub
		m_tvTitle = (TextView) findViewById(R.id.tv_title);
		ImageView _iv = (ImageView) findViewById(R.id.iv_back);
		_iv.setOnClickListener(this);

		_iv = (ImageView) findViewById(R.id.iv_help_menu);
		_iv.setOnClickListener(this);
		_iv.setVisibility(View.GONE);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iv_back:
			goBack();
			break;
		case R.id.iv_help_menu:
			onHelpMenu();
			break;
		default:
			break;
		}
	}

	private void onHelpMenu() {
		// TODO Auto-generated method stub
		
	}

	private void goBack() {
		// TODO Auto-generated method stub
		if (m_curFragment != null) {
			PRJFUNC.removeFragment(HistoryActivity.this, m_curFragment);
		} else {
			finish();
			overridePendingTransition(R.anim.hold, R.anim.right_out);
		}
	}
	
	@Override
	protected void onDestroy() {
		PRJFUNC.closeProgress(mContext);

		ActivityTask.INSTANCE.remove(this);

		releaseValues();

		super.onDestroy();
	}

	private void releaseValues() {
		m_settingsFragment = null;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goBack();
			return false;
		}

		return super.onKeyDown(keyCode, event);
	}

}
