package com.ugs.braci.history;

import java.util.zip.Inflater;

import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

public class HistoryActivityFragment extends android.support.v4.app.Fragment {
	
	private HistoryActivity mActivity;
	
	private LinearLayout m_historylistView;
	
	private TextView historyDate, historyDesc, historyTime;
	private ImageView historySoundIc;
	public static String signal;
	
	private static String date = "", desc = "", time = "", dateDect = "" ,month = "", shwDate = "" ;
	private static String prev_date="";
	public static String[] dateDectArr;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View v = inflater.inflate(R.layout.activity_history, container,
				false);
		
		mActivity = (HistoryActivity) getActivity();
		
		initValue(v);
		historyListView();
		
		// - update position
				if (!PRJFUNC.DEFAULT_SCREEN) {
					
				}
		
		return v;
		
	}

	private void initValue(View v) {
		// TODO Auto-generated method stub
		m_historylistView = (LinearLayout)v.findViewById(R.id.historyList);
		
		}
	
	
	
	
	
	String dateStr = "", prevDateStr= "", timeStr ="", histDesc = "", dateDay= "", dateMon= "", dateToShow = "" ;
	private void historyListView() {
		// TODO Auto-generated method stub
		LayoutInflater inflatorHistoryVw = (LayoutInflater)mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		int listValue = mActivity.detectedDate.size();
		
		if(listValue != 0)
		{
			
			String[] dateAry = mActivity.detectedDate.get(0).split(" ");
			
				prevDateStr = dateAry[0];
				String[] shwDate = prevDateStr.split("-");
				dateDay = shwDate[0];
				dateMon = shwDate[1];
				dateToShow = "\t"+dateDay+"\n"+dateMon;
			    timeStr = dateAry[1];
			    
			View listVw =  inflatorHistoryVw.inflate(R.layout.list_item_history, null, false);
			historyDate = (TextView)listVw.findViewById(R.id.tv_Historydate);
			historyDesc = (TextView)listVw.findViewById(R.id.tv_historyDesc);
			historyTime = (TextView)listVw.findViewById(R.id.tv_historyTime);
			historySoundIc = (ImageView)listVw.findViewById(R.id.iv_soundIcon);
			
			histDesc = mActivity.detectedSound.get(0)+" detected at ";
			historyDate.setText(dateToShow);
			historyDesc.setText(histDesc);
			historyTime.setText(timeStr);
			m_historylistView.addView(listVw);
			
			setSoundIcon(historySoundIc,mActivity.detectedSound.get(0));
			for(int i=1;i<listValue;i++)
		{
		View listVw1 =  inflatorHistoryVw.inflate(R.layout.list_item_history, null, false);
		historyDate = (TextView)listVw1.findViewById(R.id.tv_Historydate);
		historyDesc = (TextView)listVw1.findViewById(R.id.tv_historyDesc);
		historyTime = (TextView)listVw1.findViewById(R.id.tv_historyTime);
		historySoundIc = (ImageView)listVw1.findViewById(R.id.iv_soundIcon);
		String[] dateAry1 = mActivity.detectedDate.get(i).split("-");
		String newprevDateStr = dateAry1[0];
	    String newtimeStr = dateAry1[1];
		
		
		if(prevDateStr.equalsIgnoreCase(newprevDateStr))
		{
			historyDate.setVisibility(View.INVISIBLE);
			histDesc = mActivity.detectedSound.get(i)+" detected at ";
			historyDate.setText(dateToShow);
			historyDesc.setText(histDesc);
			historyTime.setText(timeStr);
			m_historylistView.addView(listVw);
		}
		else
		{
			prevDateStr = newprevDateStr;
			historyDate.setVisibility(View.VISIBLE);
			histDesc = mActivity.detectedSound.get(i)+" detected at ";
			historyDate.setText(dateToShow);
			historyDesc.setText(histDesc);
			historyTime.setText(timeStr);
			m_historylistView.addView(listVw);
		}
		
//		historyDesc.setText(desc);
//		signal = historyDesc.getText().toString();
//		historyTime.setText(time);
//	
//		m_historylistView.addView(listVw1);
		
		
		
		}
		
		
		
	}
		
}

	private void setSoundIcon(ImageView historySoundIc2, String string) {
		// TODO Auto-generated method stub
		if (signal == "DOOR BELL") {
			historySoundIc.setImageResource(R.drawable.center_detected);
			
		} else if (signal == "BACKDOOR BELL") {
			historySoundIc.setImageResource(R.drawable.center_detected);

		} else if (signal == "FIRE_ALARM"
				|| signal == "CO2") {
			historySoundIc.setImageResource(R.drawable.center_detected_fire);
		} else if (signal == "SMOKE ALARM") {
			historySoundIc.setImageResource(R.drawable.center_detected_fire);
		} else if (signal == "BABY CRYING") {
			historySoundIc.setImageResource(R.drawable.center_detected_baby);
		} else if (signal == "MOBILE RINGING") {
			historySoundIc.setImageResource(R.drawable.center_detected_phonering);
		} else if (signal == "CAR HORN") {
			historySoundIc.setImageResource(R.drawable.center_detected_carhorn);
		} else if (signal == "THIEF ALARM") {
			historySoundIc.setImageResource(R.drawable.center_detected_thief);
		} else if (signal == "ALARM CLOCK") {
			historySoundIc.setImageResource(R.drawable.center_detected_alarm);
		} else if (signal == "WAKE UP") {
			historySoundIc.setImageResource(R.drawable.center_paging_wakeup);
		} else if (signal == "BED TIME") {
			historySoundIc.setImageResource(R.drawable.center_paging_bedtime);
		} else if (signal == "SOS") {
			historySoundIc.setImageResource(R.drawable.center_paging_sos);
		} else if (signal == "MEAL TIME") {
			historySoundIc.setImageResource(R.drawable.center_paging_eattime);
		} else if (signal == "NO") {
			historySoundIc.setImageResource(R.drawable.center_paging_no);
		} else if (signal == "YES") {
			historySoundIc.setImageResource(R.drawable.center_paging_yes);
		} else if (signal == "CALLING YOU") {
			historySoundIc.setImageResource(R.drawable.center_paging_calling);
		} else if (signal == "INTERCOM") {
			historySoundIc.setImageResource(R.drawable.center_detected_intercom);
		} else if (signal == "DOG PARK") {
			historySoundIc.setImageResource(R.drawable.center_detected_dogpark);
		} else if (signal == "MICROWAVE") {
			historySoundIc.setImageResource(R.drawable.center_detected_microwave);
		} else if (signal == "TRAFFIC TONE") {
			historySoundIc.setImageResource(R.drawable.center_traffic_tone);
		} else if (signal == "TRAIN WHISTLE") {
			historySoundIc.setImageResource(R.drawable.center_train_whistle);
		} else if (signal == "POLICE SIREN") {
			historySoundIc.setImageResource(R.drawable.center_detected_police);
		} else if (signal == "SNORE") {
			historySoundIc.setImageResource(R.drawable.center_detected_snoring);
		}
	}
}
