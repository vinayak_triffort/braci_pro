package com.ugs.braci.falldetection;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;
import com.ugs.braci.settings.AdvancedSettingsActivity;
import com.ugs.braci.settings.AdvancedSettingsFragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class FallDetection extends FragmentActivity implements OnClickListener {
	
	private Context mContext;
	
	public FallDetectionFragment m_settingsFragment;
	public Fragment m_curFragment;

	public TextView m_tvTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);
		ActivityTask.INSTANCE.add(this);

		mContext = this;
		
		initValues();

		updateLCD();
		
		m_tvTitle.setText(getString(R.string.menu_fallDetection));

		m_settingsFragment = new FallDetectionFragment();
		PRJFUNC.addFragment(FallDetection.this, m_settingsFragment);

		m_curFragment = null;
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.iv_back:
			goBack();
			break;
		case R.id.iv_help_menu:
			onHelpMenu();
			break;
		default:
			break;
		}
	}
	
	private void updateLCD() {
		// TODO Auto-generated method stub
		m_tvTitle = (TextView) findViewById(R.id.tv_title);
		// PRJFUNC.setTextViewFont(mContext, m_tvTitle,
		// PRJCONST.FONT_AuctionGothicBold);

		ImageView _iv = (ImageView) findViewById(R.id.iv_back);
		_iv.setOnClickListener(this);

		_iv = (ImageView) findViewById(R.id.iv_help_menu);
		_iv.setOnClickListener(this);
		_iv.setVisibility(View.GONE);

	}

	private void initValues() {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	protected void onDestroy() {
		PRJFUNC.closeProgress(mContext);

		ActivityTask.INSTANCE.remove(this);

		releaseValues();

		super.onDestroy();
	}

	private void releaseValues() {
		m_settingsFragment = null;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goBack();
			return false;
		}

		return super.onKeyDown(keyCode, event);
	}


	private void goBack() {
		// TODO Auto-generated method stub
		if (m_curFragment != null) {
			PRJFUNC.removeFragment(FallDetection.this, m_curFragment);
		} else {
			finish();
			overridePendingTransition(R.anim.hold, R.anim.right_out);
		}
	}



	private void onHelpMenu() {
		// TODO Auto-generated method stub
		
	}

	
}
