package com.ugs.braci.falldetection;

import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class FallDetectionFragment extends Fragment {
	
	private FallDetection mActivity;
	
	private LinearLayout frmPower;
	private RelativeLayout frmSensitivity , frmTimer , frmSms, frmEmail, frmPhone;
	
	

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.activity_fall_detection, container,
				false);
		
		mActivity = (FallDetection) getActivity();
		
		initValue(v);
		
		// - update position
				if (!PRJFUNC.DEFAULT_SCREEN) {
					
				}
		
		return v;
}




	private void initValue(View v) {
		// TODO Auto-generated method stub
		
		frmPower = (LinearLayout)v.findViewById(R.id.frm_power);
		frmSensitivity = (RelativeLayout)v.findViewById(R.id.frm_setting);
		frmTimer = (RelativeLayout)v.findViewById(R.id.frm_setting1);
		frmSms = (RelativeLayout)v.findViewById(R.id.frm_sms);
		frmPhone = (RelativeLayout)v.findViewById(R.id.frm_phone);
		frmEmail = (RelativeLayout)v.findViewById(R.id.frm_email);
		
	}

}