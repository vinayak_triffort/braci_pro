package com.ugs.braci.photoslider;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ugs.braci.R;

class PhotoSliderAdapter extends FragmentPagerAdapter {
	protected static final int[] CONTENT = new int[] { R.drawable.photo1,
			R.drawable.photo2, R.drawable.photo3, R.drawable.photo4 };

	private int mCount = CONTENT.length;

	public PhotoSliderAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		return PhotoFragment.newInstance(CONTENT[position % CONTENT.length]);
	}

	@Override
	public int getCount() {
		return mCount;
	}

	public void setCount(int count) {
		if (count > 0 && count <= 10) {
			mCount = count;
			notifyDataSetChanged();
		}
	}
}