package com.ugs.braci.photoslider;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ugs.braci.R;
import com.ugs.braci.login.LoginActivity;
import com.ugs.braci.login.SignupActivity;

public final class PhotoFragment extends Fragment implements OnClickListener {
	private static final String KEY_IMAGE_ID = "img_res_id";

	public static PhotoFragment newInstance(int nResId) {
		PhotoFragment fragment = new PhotoFragment();

		fragment.m_nImgResId = nResId;

		return fragment;
	}

	private int m_nImgResId = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if ((savedInstanceState != null)
				&& savedInstanceState.containsKey(KEY_IMAGE_ID)) {
			m_nImgResId = savedInstanceState.getInt(KEY_IMAGE_ID);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_photo, container,
				false);
		
		
		updateLCD(v);

		return v;
	}

	private void updateLCD(View v) {
		ImageView imgView = (ImageView) v.findViewById(R.id.iv_image);
		imgView.setImageResource(m_nImgResId);

		ImageView _iv = (ImageView) v.findViewById(R.id.iv_btn_signin);
		_iv.setOnClickListener(this);
		
		_iv = (ImageView) v.findViewById(R.id.iv_btn_signup);
		_iv.setOnClickListener(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_IMAGE_ID, m_nImgResId);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_btn_signin:
			goToLoginActivity();
			break;

		case R.id.iv_btn_signup:
			goToSignupActivity();
			break;
		default:
			break;
		}
	}

	private void goToLoginActivity() {
		Intent intent = new Intent(getActivity(), LoginActivity.class);
		getActivity().startActivity(intent);
		getActivity().finish();
	}
	
	private void goToSignupActivity() {
		Intent intent = new Intent(getActivity(), SignupActivity.class);
		intent.putExtra(SignupActivity.PARAM_FROM, SignupActivity.FROM_PHOTOSLIDING);
		getActivity().startActivity(intent);
		getActivity().finish();
	}		

}
