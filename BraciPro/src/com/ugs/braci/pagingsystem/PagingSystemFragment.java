package com.ugs.braci.pagingsystem;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.PRJFUNC.Signal;
import com.ugs.braci.DoorbellDetectedActivity;
import com.ugs.braci.R;
import com.ugs.braci.adapter.UIListAdapter;
import com.ugs.info.UIListItemInfo;
import com.ugs.service.BraciProService;

public class PagingSystemFragment extends Fragment {

	private PagingSystemActivity mActivity;

	private TextView m_tvDescription;
	
	private ListView m_listItems;
	private UIListAdapter m_adapterItems;
	

	private final int ITEM_PAGING_SOS = 0;
	private final int ITEM_PAGING_EAT = 1;
	private final int ITEM_PAGING_BED = 2;
	private final int ITEM_PAGING_YES = 3;
	private final int ITEM_PAGING_NO = 4;
	private final int ITEM_PAGING_WAKEUP = 5;
	private final int ITEM_PAGING_CALL = 6;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_general, container,
				false);

		mActivity = (PagingSystemActivity) getActivity();

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			
		}

		return v;
	}

	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		itemInfo = new UIListItemInfo(R.drawable.ic_paging_sosbutton,
				mActivity.getString(R.string.paging_sos_title),
				mActivity.getString(R.string.paging_sos_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_paging_eattime,
				mActivity.getString(R.string.paging_meal_title),
				mActivity.getString(R.string.paging_eat_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_paging_bedtime,
				mActivity.getString(R.string.paging_bed_title),
				mActivity.getString(R.string.paging_bed_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_paging_yes,
				mActivity.getString(R.string.paging_yes_title),
				mActivity.getString(R.string.paging_yes_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_paging_no,
				mActivity.getString(R.string.paging_no_title),
				mActivity.getString(R.string.paging_no_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);
		
		itemInfo = new UIListItemInfo(R.drawable.ic_paging_wakeup,
				mActivity.getString(R.string.paging_wakeup_title),
				mActivity.getString(R.string.paging_wakeup_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);
		
		itemInfo = new UIListItemInfo(R.drawable.ic_paging_call,
				mActivity.getString(R.string.paging_call_title),
				mActivity.getString(R.string.paging_call_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
		m_tvDescription.setVisibility(View.GONE);

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UIListAdapter(mActivity, R.layout.list_item_pagingsystem,
				new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		refreshList();
	}

	UIListAdapter.Callback m_adapterCallback = new UIListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			Signal signal = Signal.SOS;
			switch(p_nPosition) {
			case ITEM_PAGING_SOS:
				signal = Signal.SOS;
				break;
			case ITEM_PAGING_EAT:
				signal = Signal.MEAL_TIME;
				break;
			case ITEM_PAGING_BED:
				signal = Signal.BED_TIME;
				break;
			case ITEM_PAGING_YES:
				signal = Signal.YES;
				break;
			case ITEM_PAGING_NO:
				signal = Signal.NO;
				break;
			case ITEM_PAGING_WAKEUP:
				signal = Signal.WAKE_UP;
				break;
			case ITEM_PAGING_CALL:
				signal = Signal.CALLING_YOU;
				break;
			}
			
			// PRJFUNC.sendSignalToPebble(mActivity, signal);
			PRJFUNC.DETECTED_NUMBER = 0;
			PRJFUNC.sendPushMessage(mActivity, signal);
			// createActivity(signal);
			
			Toast.makeText(mActivity, mActivity.getString(R.string.paging_sent_success), Toast.LENGTH_SHORT).show();
		}
	};

}
