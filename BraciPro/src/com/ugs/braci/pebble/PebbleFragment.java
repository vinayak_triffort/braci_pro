package com.ugs.braci.pebble;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;
import com.ugs.braci.adapter.UIListAdapter;
import com.ugs.info.UIListItemInfo;

public class PebbleFragment extends Fragment {

	private PebbleActivity mActivity;

	private ListView m_listItems;
	private UIListAdapter m_adapterItems;

	private TextView m_tvDescription;
	
	private final int ITEM_PEBBLE_TEST_NOTIFICATIONS = 0;
	private final int ITEM_PEBBLE_DOWNLOAD = 1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_general, container,
				false);

		mActivity = (PebbleActivity) getActivity();

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			
		}

		return v;
	}

	public void refreshList() {
		m_adapterItems.clear();

		UIListItemInfo itemInfo = null;

		itemInfo = new UIListItemInfo(R.drawable.ic_pebble_testnoti,
				mActivity.getString(R.string.pebble_test_title),
				mActivity.getString(R.string.pebble_test_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);

		itemInfo = new UIListItemInfo(R.drawable.ic_pebble_download,
				mActivity.getString(R.string.pebble_download_title),
				mActivity.getString(R.string.pebble_download_desc), false);
		itemInfo.setChecked(false);
		m_adapterItems.add(itemInfo);
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		m_tvDescription = (TextView) v.findViewById(R.id.tv_desc);
		m_tvDescription.setVisibility(View.GONE);

		m_listItems = (ListView) v.findViewById(R.id.listview);
		m_adapterItems = new UIListAdapter(mActivity, R.layout.list_item_title_and_desc,
				new ArrayList<UIListItemInfo>());
		m_adapterItems.setCallback(m_adapterCallback);
		m_listItems.setAdapter(m_adapterItems);

		refreshList();
	}

	UIListAdapter.Callback m_adapterCallback = new UIListAdapter.Callback() {
		@Override
		public void onItemClick(int p_nPosition) {
			switch(p_nPosition) {
			case ITEM_PEBBLE_TEST_NOTIFICATIONS:
				mActivity.onPebbleTestNoti();
				break;
			case ITEM_PEBBLE_DOWNLOAD:
				onDownloadPebble();
				break;
			}
		}
	};
	
	private void onDownloadPebble() {
		File f = new File(Environment.getExternalStorageDirectory() + "/Braci.pbw");
		//if (!f.exists()) {
			try {

				InputStream is = mActivity.getAssets().open("Braci.pbw");
				int size = is.available();
				byte[] buffer = new byte[size];
				is.read(buffer);
				is.close();

				FileOutputStream fos = new FileOutputStream(f);
				fos.write(buffer);
				fos.close();
			} catch (Exception e) {
				return;
			}
		// }

		MimeTypeMap myMime = MimeTypeMap.getSingleton();

		Intent newIntent = new Intent(android.content.Intent.ACTION_VIEW);

		// Intent newIntent = new Intent(Intent.ACTION_VIEW);
		String mimeType = myMime.getMimeTypeFromExtension(fileExt(f.toString())
				.substring(1));
		newIntent.setDataAndType(Uri.fromFile(f), "pebble");
		newIntent.setFlags(newIntent.FLAG_ACTIVITY_NEW_TASK);
		try {
			startActivity(newIntent);
		} catch (android.content.ActivityNotFoundException e) {
			Toast.makeText(mActivity, getResources().getString(R.string.toast1),
					4000).show();
		}
	}

	private String fileExt(String url) {
		if (url.indexOf("?") > -1) {
			url = url.substring(0, url.indexOf("?"));
		}
		if (url.lastIndexOf(".") == -1) {
			return null;
		} else {
			String ext = url.substring(url.lastIndexOf("."));
			if (ext.indexOf("%") > -1) {
				ext = ext.substring(0, ext.indexOf("%"));
			}
			if (ext.indexOf("/") > -1) {
				ext = ext.substring(0, ext.indexOf("/"));
			}
			return ext.toLowerCase();

		}
	}

}
