package com.ugs.braci.pebble;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.PRJFUNC.Signal;
import com.ugs.braci.R;

public class PebbleTestFragment extends Fragment implements OnClickListener {

	private PebbleActivity mActivity;

	private Fragment m_oldFragment;
	private String m_strOldTitle;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		final View v = inflater.inflate(R.layout.fragment_pebble_test,
				container, false);

		mActivity = (PebbleActivity) getActivity();

		m_oldFragment = mActivity.m_curFragment;
		m_strOldTitle = mActivity.m_tvTitle.getText().toString();

		mActivity.m_curFragment = PebbleTestFragment.this;

		mActivity.m_tvTitle.setText(mActivity
				.getString(R.string.pebble_test_title));

		if (m_oldFragment == null) {
			PRJFUNC.hideFragment(mActivity, mActivity.m_pebbleFragment);
		} else {
			PRJFUNC.hideFragment(mActivity, m_oldFragment);
		}

		updateLCD(v);

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {

		}

		return v;
	}

	@Override
	public void onDestroyView() {
		if (mActivity.m_curFragment == PebbleTestFragment.this) {
			mActivity.m_curFragment = m_oldFragment;
			mActivity.m_tvTitle.setText(m_strOldTitle);

			if (mActivity.m_curFragment == null) {
				PRJFUNC.showFragment(mActivity, mActivity.m_pebbleFragment);
			} else {
				PRJFUNC.showFragment(mActivity, m_oldFragment);
			}
		}

		super.onDestroyView();
	}

	// //////////////////////////////////////////////////
	private void updateLCD(View v) {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mActivity);
		}

		ImageView _iv = (ImageView) v.findViewById(R.id.iv_test_firealarm);
		_iv.setOnClickListener(this);

		_iv = (ImageView) v.findViewById(R.id.iv_test_doorbell);
		_iv.setOnClickListener(this);

		_iv = (ImageView) v.findViewById(R.id.iv_test_landline);
		_iv.setOnClickListener(this);

		_iv = (ImageView) v.findViewById(R.id.iv_test_carhorn);
		_iv.setOnClickListener(this);

		_iv = (ImageView) v.findViewById(R.id.iv_test_intercom);
		_iv.setOnClickListener(this);

		_iv = (ImageView) v.findViewById(R.id.iv_test_alarmclock);
		_iv.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		
		case R.id.iv_test_firealarm:
			PRJFUNC.sendSignalToPebble(mActivity, Signal.FIRE_ALARM);
			break;
			
		case R.id.iv_test_doorbell:
			PRJFUNC.sendSignalToPebble(mActivity, Signal.DOOR_BELL);
			break;
			
		case R.id.iv_test_landline:
			PRJFUNC.sendSignalToPebble(mActivity, Signal.MOBILE_RINGING);
			break;
			
		case R.id.iv_test_carhorn:
			PRJFUNC.sendSignalToPebble(mActivity, Signal.CAR_HORN);
			break;
			
		case R.id.iv_test_intercom:
			PRJFUNC.sendSignalToPebble(mActivity, Signal.INTERCOM);
			break;
			
		case R.id.iv_test_alarmclock:
			PRJFUNC.sendSignalToPebble(mActivity, Signal.ALARM_CLOCK);
			break;

		default:
			break;
		}
	}
}
