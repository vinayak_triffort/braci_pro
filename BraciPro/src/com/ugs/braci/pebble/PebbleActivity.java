package com.ugs.braci.pebble;

import android.R.bool;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;
import com.ugs.braci.setup.FriedLandSubFragment;
import com.ugs.braci.setup.SetupActivity;

public class PebbleActivity extends FragmentActivity implements OnClickListener {

	private Context mContext;
	
	public static String PARAM_DIRECT_TEST = "direct_test";

	public PebbleFragment m_pebbleFragment;
	public Fragment m_curFragment;

	public TextView m_tvTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setup);
		ActivityTask.INSTANCE.add(this);

		mContext = this;

		initValues();

		updateLCD();
		
		boolean bDirectTestNoti = getIntent().getBooleanExtra(PARAM_DIRECT_TEST, false);

		m_tvTitle.setText(getString(R.string.menu_pebble));

		m_pebbleFragment = new PebbleFragment();
		PRJFUNC.addFragment(PebbleActivity.this, m_pebbleFragment);

		m_curFragment = null;
		if (bDirectTestNoti) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					onPebbleTestNoti();
				}
			}, 100);
		}
	}

	private void initValues() {

	}

	@Override
	protected void onDestroy() {
		PRJFUNC.closeProgress(mContext);

		ActivityTask.INSTANCE.remove(this);

		releaseValues();

		super.onDestroy();
	}

	private void releaseValues() {
		m_pebbleFragment = null;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			goBack();
			return false;
		}

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	private void updateLCD() {
		m_tvTitle = (TextView) findViewById(R.id.tv_title);
		// PRJFUNC.setTextViewFont(mContext, m_tvTitle,
		// PRJCONST.FONT_AuctionGothicBold);

		ImageView _iv = (ImageView) findViewById(R.id.iv_back);
		_iv.setOnClickListener(this);

		_iv = (ImageView) findViewById(R.id.iv_help_menu);
		_iv.setOnClickListener(this);
		_iv.setVisibility(View.GONE);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_back:
			goBack();
			break;
		case R.id.iv_help_menu:
			onHelpMenu();
			break;
		default:
			break;
		}
	}

	private void onHelpMenu() {
		// TODO Auto-generated method stub

	}

	public void goBack() {
		if (m_curFragment != null) {
			PRJFUNC.removeFragment(PebbleActivity.this, m_curFragment);
		} else {
			finish();
			overridePendingTransition(R.anim.hold, R.anim.right_out);
		}
	}

	public void onPebbleTestNoti() {
		PebbleTestFragment fragment = new PebbleTestFragment();
		PRJFUNC.addFragment(PebbleActivity.this, fragment);
	}

}
