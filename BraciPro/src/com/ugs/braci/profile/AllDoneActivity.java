package com.ugs.braci.profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.ActivityTask;
import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;

public class AllDoneActivity extends Activity implements OnClickListener {

	private final String TAG = "_RecordDoneActivity";

	private Context mContext;

	private int m_nRecordType;

	// ////////////////////////////////////////////////////////////
	// //////////////////////////////////////////////////////////////
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_done);
		ActivityTask.INSTANCE.add(this);

		mContext = (Context) this;

		updateLCD();

		// - update position
		if (!PRJFUNC.DEFAULT_SCREEN) {
			scaleView();
		}
	}

	@Override
	protected void onDestroy() {
		releaseValues();

		ActivityTask.INSTANCE.remove(this);

		super.onDestroy();
	}

	private void releaseValues() {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// onBack();
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	// //////////////////////////////////////////////////
	private void updateLCD() {

		if (PRJFUNC.mGrp == null) {
			PRJFUNC.resetGraphValue(mContext);
		}

		ImageView _iv = (ImageView) findViewById(R.id.iv_btn);
		_iv.setOnClickListener(this);

		TextView _tvContent = (TextView) findViewById(R.id.tv_content);
		_tvContent.setText(getString(R.string.all_done));

		TextView _tvOk = (TextView) findViewById(R.id.tv_btn_text);
		_tvOk.setText(getString(R.string.word_ok));
	}

	private void scaleView() {
		if (PRJFUNC.mGrp == null) {
			return;
		}
	}

	// /////////////////////////////////////
	private void onBack() {
		Intent intent = new Intent();
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_btn:
			onBack();
			break;

		default:
			break;
		}

	}
}
