package com.ugs.service;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Message;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;
import com.uc.prjcmn.PRJFUNC;
import com.uc.prjcmn.PRJFUNC.Signal;
import com.ugs.braci.GlobalValues;

public class PushReceiver extends ParsePushBroadcastReceiver {
	private static final String TAG = "MyCustomReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			if (GlobalValues.m_bDetectionPhone)
				return;
			String action = intent.getAction();
			String channel = intent.getExtras().getString("com.parse.Channel");
			JSONObject json = new JSONObject(intent.getExtras().getString(
					"com.parse.Data"));

			Signal signal = Signal.DOOR_BELL;
			try {
				String strMessage = json.getString("message");
				Log.d(TAG, "got action " + action + " on channel " + channel
						+ " with:" + strMessage);
				
				String[] strSoundsInfos = strMessage.split(",");
				String strDevId = strSoundsInfos[0];
				if (PRJFUNC.getDeviceID(context).equals(strDevId)) // 이 기기가 보낸 메시지는 처리하지 않음.
					return;
				
				PRJFUNC.DETECTED_TYPE = Integer.parseInt(strSoundsInfos[1]);
				PRJFUNC.DETECTED_NUMBER = Integer.parseInt(strSoundsInfos[2]);
				signal = PRJFUNC.ConvIntToSignal(PRJFUNC.DETECTED_TYPE);
			} catch (Exception e) {
				return;
			}

			if (GlobalValues._myService.m_handler != null) {
				Message msg = new Message();
				msg.what = GlobalValues.COMMAND_OPEN_ACITIVITY;
				msg.obj = signal;
				GlobalValues._myService.m_handler.sendMessage(msg);
			}

		} catch (JSONException e) {
			Log.d(TAG, "JSONException: " + e.getMessage());
		}
	}
	
}