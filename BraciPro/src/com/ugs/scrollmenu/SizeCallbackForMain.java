package com.ugs.scrollmenu;

import com.ugs.scrollmenu.MyHorizontalScrollView.SizeCallback;

public class SizeCallbackForMain implements SizeCallback {
    int[] m_widths;

    public SizeCallbackForMain(int[] widths) {
        super();
        
    	m_widths = widths;
    }
    
    @Override
    public void onGlobalLayout() {
        
    }

	@Override
	public void getViewSize(int idx, int h, int[] dims) {
		// TODO Auto-generated method stub
    	if (m_widths != null && idx < m_widths.length)
    		dims[0] = m_widths[idx];
    	else
    		dims[0] = 0;
    	
        dims[1] = h;		
	}
}