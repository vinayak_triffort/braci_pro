package com.ugs.scrollmenu;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.HorizontalScrollView;

public class ClickListenerForScrolling implements OnClickListener {
	MyHorizontalScrollView scrollView;
	View menu;
	/**
	 * Menu must NOT be out/shown to start with.
	 */
	boolean menuOut = false;
	boolean leftMenu = true;
	
	int m_nCurIdx = 0;

	public interface CallbackEvent {
		public void callbackMethod(boolean bOpened);
	}

	CallbackEvent m_cbOnMenuOpened = null;

	public ClickListenerForScrolling(MyHorizontalScrollView p_scrollView,
			View p_menu, int p_nCurIdx) {
		super();
		this.scrollView = p_scrollView;
		this.menu = p_menu;
		this.m_nCurIdx = p_nCurIdx;
	}

	public void setCbOnMenuOpened(CallbackEvent p_cbEvent) {
		m_cbOnMenuOpened = p_cbEvent;
	}

	@Override
	public void onClick(View v) {
		int menuWidth = menu.getMeasuredWidth();
		menu.setVisibility(View.VISIBLE);
		if (!menuOut) {
			open();
		} else {
			close();
		}
		
	}

	public void open() {
		if (m_cbOnMenuOpened != null) {
			m_cbOnMenuOpened.callbackMethod(true);
		}
		
		scrollView.setPosToView(m_nCurIdx, false);		
		menuOut = true;	
	}
	
	public void close() {
		// Scroll to menuWidth so menu isn't on screen.
		if (menuOut == false)
			return;
		
		if (m_cbOnMenuOpened != null) {
			m_cbOnMenuOpened.callbackMethod(false);
		}
		
		scrollView.setPosToView(1, false);
		menuOut = false;
	}

	public void reset() {
		menuOut = false;
	}
}