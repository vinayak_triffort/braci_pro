package com.uc.prjcmn;

public interface PRJCONST {
	public static final boolean IsReleaseVersion = true;
	public static final boolean IsTestVersion = false;
	public static final boolean IsLimitedVersion = false;
	public static final boolean IsUnversalEngine = false;
	public static final boolean IsInternetVersion = true;
	public static final boolean IsBabyCryingVersion = false;
	public static final boolean IsIgnoreExpiration = true;
	
	public static final int FREE_USE_DAYS = 30;
	
	public static final int DB_VERSION = 2;

	// -- Screen size --
	final static int SCREEN_WIDTH = 720;
	final static int SCREEN_HEIGHT = 1280;
	final static int SCREEN_DPI = 320;

	final static String FONT_OpenSans = "OpenSans-Light.ttf";
	final static String FONT_OpenSansBold = "OpenSans-Bold.ttf";

	final static int RECORDING_PROGRESS = 30000;
	final static int RECORD_STOPPED = 30001;

	final static int MAX_FIRE_SOUND_CNT = 50;

	public static final int SOUND_GENERAL = 0;
	public static final int SOUND_DINGDONG = 1;
	public static final int SOUND_METABELL = 2;
	public static final int SOUND_BUZZER = 3;
	public static final int SOUND_MELODY = 4;

	// Record sound type
	public static final int REC_SOUND_TYPE_DOORBELL = 0;
	public static final int REC_SOUND_TYPE_BACKDOORBELL = 1;
	public static final int REC_SOUND_TYPE_SMOKEALARM = 2;
	public static final int REC_SOUND_TYPE_LINEPHONE = 3;
	public static final int REC_SOUND_TYPE_INTERCOM = 4;
	public static final int REC_SOUND_TYPE_CO2 = 5;
	public static final int REC_SOUND_TYPE_ALARMCLOCK = 6;
	public static final int REC_SOUND_TYPE_THEFTALARM = 7;
	public static final int REC_SOUND_TYPE_MICROWAVE = 8;
	public static final int REC_SOUND_TYPE_CNT = 9;
	
	// Universal engine type
	public static final int UNIVERSAL_ENGINE_BABYCRYING = 0;
	public static final int UNIVERSAL_ENGINE_SMOKEALARM = 1;
	public static final int UNIVERSAL_ENGINE_CO2 = 2;
	public static final int UNIVERSAL_ENGINE_CNT = 3;

	// UI List Item Type
	public static final int UI_LIST_ITEM_TYPE_1 = 0;
	public static final int UI_LIST_ITEM_TYPE_2 = 1;

	public static final int FIX_FALSE_ALARM = 0;
	public static final int FIX_MISSING_ALARM = 1;

	// Address Type
	public static final int ADDRESS_TYPE_HOME = 0;
	public static final int ADDRESS_TYPE_OFFICE = 1;
	
	// Invalid value of location
	public static final double INVALID_LOCATION_VALUE = -999.0;
	
	// Profile Modes
	public static final int PROFILE_MODE_HOME = 0;
	public static final int PROFILE_MODE_OFFICE = 1;
	public static final int PROFILE_MODE_DRIVE = 2;
	public static final int PROFILE_MODE_WALK = 3;
	
	// Deaf products identifications
	public static final int DEAF_BELLMAN = 0;
	public static final int DEAF_BYRON_BYRON = 1;
	public static final int DEAF_BYRON_BYRONPLUGIN = 2;
	public static final int DEAF_FRIEDLAND_LIBRA = 3;
	public static final int DEAF_FRIEDLAND_EVO = 4;
	public static final int DEAF_FRIEDLAND_PLUGIN = 5;
	public static final int DEAF_GREENBROOK_GREENBROOK = 6;
	public static final int DEAF_ECHO_ECHOCHIME = 7;
	public static final int DEAF_GEEMARK_CL1 = 8;
	public static final int DEAF_GEEMARK_CL2 = 9;
	public static final int DEAF_GEEMARK_AMPLICALL = 10;
	public static final int DEAF_AMPLICOM_AMPLICOM = 11;
	public static final int DEAF_TYPE_CNT = 12;
}
