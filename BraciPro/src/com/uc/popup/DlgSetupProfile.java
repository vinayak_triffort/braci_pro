package com.uc.popup;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.uc.prjcmn.PRJCONST;
import com.uc.prjcmn.PRJFUNC;
import com.ugs.braci.R;

public class DlgSetupProfile extends Activity implements OnClickListener {

	public static final String PARAM_HOME_EXIST = "home_exist";
	public static final String PARAM_OFFICE_EXIST = "office_exist";
	
	public static final String RESULT_STR = "SELECT_ITEM";
	
	private int m_nOption = PRJCONST.PROFILE_MODE_HOME;
	
	private boolean m_bHomeExist = false;
	private boolean m_bOfficeExist = false;

	private ImageView m_ivHome;
	private ImageView m_ivOffice;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		getWindow().setFlags(WindowManager.LayoutParams.ALPHA_CHANGED,
				WindowManager.LayoutParams.ALPHA_CHANGED);
		getWindow().setGravity(Gravity.CENTER);

		setContentView(R.layout.dlg_setup_profile);
		
		m_bHomeExist = getIntent().getBooleanExtra(PARAM_HOME_EXIST, false);
		m_bOfficeExist = getIntent().getBooleanExtra(PARAM_OFFICE_EXIST, false);

		updateLCD();
		
		updateUIOptions();
	}

	@Override
	public void onBackPressed() {
		onCancel();

		super.onBackPressed();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.frm_home_mode:
			m_nOption = PRJCONST.PROFILE_MODE_HOME;
			updateUIOptions();
			break;
		case R.id.frm_office_mode:
			m_nOption = PRJCONST.PROFILE_MODE_OFFICE;
			updateUIOptions();
			break;
		case R.id.tv_ok:
			onSelect();
			break;
		}
	}

	private void updateUIOptions() {
		m_ivHome.setSelected(m_nOption == PRJCONST.PROFILE_MODE_HOME);
		m_ivOffice.setSelected(m_nOption == PRJCONST.PROFILE_MODE_OFFICE);
	}

	/**
	 * 
	 */
	private void updateLCD() {

		View frm = findViewById(R.id.frm_home_mode);
		frm.setOnClickListener(this);
		m_ivHome = (ImageView) findViewById(R.id.iv_opt_home);
		TextView _tv = (TextView) findViewById(R.id.tv_home_mode);
		if (!m_bHomeExist) {
			_tv.setText("Train home mode");
		} else {
			_tv.setText("Edit home mode");
		}
		
		frm = findViewById(R.id.frm_office_mode);
		frm.setOnClickListener(this);
		m_ivOffice = (ImageView) findViewById(R.id.iv_opt_office);
		_tv = (TextView) findViewById(R.id.tv_office_mode);
		if (!m_bOfficeExist) {
			_tv.setText("Train the office mode");
		} else {
			_tv.setText("Edit the office mode");
		}

		_tv = (TextView)findViewById(R.id.tv_ok);
		_tv.setOnClickListener(this);
	}

	// ///////////////////////////////////////////
	private void onSelect() {
		Intent intent;
		intent = getIntent();
		intent.putExtra(RESULT_STR, m_nOption);
		setResult(RESULT_OK, intent);
		finish();
	}
	
	private void onCancel() {
		setResult(RESULT_CANCELED);
		finish();
	}
}
