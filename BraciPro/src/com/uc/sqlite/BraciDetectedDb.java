package com.uc.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BraciDetectedDb extends SQLiteOpenHelper {
	
	public static String dbName = "braciProDataBase";
	public static String dbTable = "history";
	public static String detectionDate = "date";
	
	
	public static String detectionSound = "soundType";

	public BraciDetectedDb(Context context){
		super(context, dbName, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("create table history(date text,soundType text)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
