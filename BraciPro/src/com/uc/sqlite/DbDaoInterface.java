package com.uc.sqlite;

import java.util.ArrayList;

import android.content.ContentValues;

/**
 * database dao class 들의 인터페이스
 * 
 * @author orient534
 */
public abstract interface DbDaoInterface {

	/** 사용 변수들 초기화 */
	public abstract void onDestroy();

	/** database row 갯수 */
	public abstract int getCount();

	/** database 초기화 */
	public abstract int clearTableDatas();

	/** insert, 추가된 row 의 id 를 리턴한다. */
	public long insertData(Object p_data);

	/** update, update 된 갯수를 리턴한다. */
	public abstract int updateData(ContentValues p_cv, String whereClause,
			String[] whereArgs);

	/** delete, delete 된 갯수를 리턴한다. */
	public abstract int deleteData(String whereClause, String[] whereArgs);

	/** 데이터 배렬을 리턴한다, 찾아지는 데이터를 모두 배렬로서 리턴한다. */
	public abstract ArrayList<Object> getDatas(String selection,
			String[] selectionArgs, String groupBy, String having,
			String orderBy);

	/** 데이터 리턴, 찾아지는 데이터가 여러개이면 처음것만 리턴한다. */
	public abstract Object getData(String selection, String[] selectionArgs,
			String groupBy, String having, String orderBy);
}
